<script type="text/javascript">
	jQuery("#salesimport").change(function(){
		formdata = new FormData();
        var file    = this.files[0];
        formdata.append("file", file);
		jQuery.ajax({
				url: '<?php echo base_url(); ?>salesadmin/salesreport/import/',
				type: 'POST',
				contentType: false,
				processData: false,
				data: formdata,
				success: function (data) {
					if(data==1){
						jQuery('#succes_div').html("Data Imported Successfully and will redirect for preview");
						jQuery('#Mymodel_success').modal('show', {backdrop: 'static'});
						window.setTimeout(function(){
							window.location.href ="<?php echo base_url(); ?>previewexcel/importpreview/importpreview";
						}, 1000);					
					}						
					else{
						alert("Upload the excel with correct format");
					}
				}
		});
	});
</script>

<?php 
public function import(){
	if($this->session->userdata('superadmin_id')){
		$fileName = time().$_FILES['file']['name'];
         
        $config['upload_path'] = './assets/'; 
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        //$config['max_size'] = 10000;
         
        $this->load->library('upload');
        $this->upload->initialize($config);
         
        if(! $this->upload->do_upload('file') ){
			echo $this->upload->display_errors();
		}        
             
        $media = $this->upload->data();
	  
       $inputFileName = './assets/'.$media['file_name'];
        
        try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
 
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
            $res = array();
            $not_exist = array();
            for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
            	
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);                                                 
             
			 
			 
			 $displayDate = PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][3], 'YYYY-MM-DD');
			 

			  $ddate2 = date('Y-m-d', strtotime(str_replace('-', '/', $rowData[0][3])));


                 $data = array(
                'bill' => $rowData[0][0],
                'category' => $rowData[0][1],
                'customer' => $rowData[0][2],
				'date' => $displayDate,
				'quantity' => round($rowData[0][4],2),
                'price' => round($rowData[0][5],2),
				'tprice' => round($rowData[0][4] * $rowData[0][5],2),
				'amount' => $rowData[0][6],
				'balance' => round(($rowData[0][4] * $rowData[0][5]),2) - round($rowData[0][6],2),
				'qtybal' => 0,
                'created_date' => date('Y-m-d H:i:s'),
				'created_by' => $this->session->userdata('superadmin_id')
				);

				array_push($res, $data);
				$check_category = $this->sales->check_exist_table(array('category_id' => $rowData[0][1]),'category');
				if($check_category['category_id']){
					$not_exist['category'][] = $check_category['category_id'];
				}
				$check_customer = $this->sales->check_exist_table(array('id' => $rowData[0][2]),'customer_login');
				if($check_customer['id']){
					$not_exist['customer'][] = $check_customer['id'];
				}
            }
            $this->session->set_userdata(array('not_exist' =>$not_exist));
            $this->session->set_userdata(array('preview_data' =>$res));
            $this->session->set_userdata(array('preview_type' =>'sales'));
            $this->session->set_userdata(array('preview_table' => "salesitem"));
            $this->session->set_userdata(array('preview_name' => "Manage Sales Excel"));
            $this->session->set_userdata(array('preview_data_col' => array("bill","category","customer","date","quantity","price","amount")));
            $this->session->set_userdata(array('redirect_url' => base_url()."salesadmin/salesreport" ));
			unlink($media['full_path']);

			//if($insert!=0)			
				echo 1;	
			
         } else{
		 redirect('../login');
		}
    
	}
?>