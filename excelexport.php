<?php function xlsPrintOrder($data,$srch){
        $CI     = get_instance();
        
       // $ORDNO       = '#'.str_pad(1, 6, '0', STR_PAD_LEFT);
		
        // Create new PHPExcel object
        $objPHPExcel            = new PHPExcel();
       // $orderProductsLists     = $CI->Settingmodel->orderProductsList($orderID);
        //$orderInfo              = $CI->Settingmodel->orderInfo($orderID);
        //$countRows              = count($orderProductsLists);

        // $Gtotal                 = $orderInfo->total;
        // $shippingPrice  = '';
        // $shippingPrice1 = 0;
        
		
        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                     ->setLastModifiedBy("Maarten Balliauw")
                                     ->setTitle("Office 2007 XLSX Test Document")
                                     ->setSubject("Office 2007 XLSX Test Document")
                                     ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                     ->setKeywords("office 2007 openxml php")
                                     ->setCategory("Test result file");


                $main_title_style = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => '31849b'),
                        'size'  => 20,
                        
                    ));
                $smalltitle_style = array(
                    'font'  => array(
                        'bold'  => true,
                        'size'  => 12
                    ),
                    'alignment' => array(
                            'wrap'       => true
                    )
                );
				
				
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true); 
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		 		 
        //////////////////////////////////
        
		if(($data=='searchitem') || ($data =='searchcus') || ($data =='supersales')) {
			if($data=='searchitem') {
				$CI->load->model('salesadmin/Searchreport_Model', 'salesexcel');	
			} else if($data =='searchcus') {
				$CI->load->model('salesadmin/Searchcustomer_Model', 'salesexcel');
			} else if($data =='supersales') {
				$CI->load->model('superadmin/Salesreport_Model', 'salesexcel');
			}
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true); 
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true); 
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		
		$srchitemlist = $CI->salesexcel->srchitemlist($srch);
		$categorylist = $CI->salesexcel->categorylist();
		$cuslist = $CI->salesexcel->cuslist();
		// echo '<pre>';
		// print_r($categorylist);die();
		
		$countRows = count($srchitemlist);
		
        $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B2:J2');
        $objPHPExcel->getActiveSheet()->getCell('B2')->setValue('Sales Item Details');  
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D3:E3');
        $objPHPExcel->getActiveSheet()->getCell('D3')->setValue('Total no of Entries');
        $objPHPExcel->getActiveSheet()->getCell('F3')->setValue($countRows);   
        $objPHPExcel->getActiveSheet()->getCell('B5')->setValue('SI Number');
        $objPHPExcel->getActiveSheet()->getCell('C5')->setValue('Bill');
        $objPHPExcel->getActiveSheet()->getCell('D5')->setValue('Category');
        $objPHPExcel->getActiveSheet()->getCell('E5')->setValue('Customer Name');
        $objPHPExcel->getActiveSheet()->getCell('F5')->setValue('Date');
		$objPHPExcel->getActiveSheet()->getCell('G5')->setValue('Price');
		$objPHPExcel->getActiveSheet()->getCell('H5')->setValue('Quantity');
		$objPHPExcel->getActiveSheet()->getCell('I5')->setValue('Total Price');
		$objPHPExcel->getActiveSheet()->getCell('J5')->setValue('Received Payment');
		$objPHPExcel->getActiveSheet()->getCell('K5')->setValue('Balance');
        
        $objPHPExcel->getActiveSheet()->getStyle('B5:C5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('D5:E5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('F5:G5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('H5:I5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('J5:K5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($main_title_style);
                
        $s          = 6;
        $i          = 1;
		$totquantity=0;
		$totprice=0;
		$totamount=0;
		$balance=0;
		foreach ($srchitemlist as $list) {
				$catname = '';
				$cusname = '';
				foreach($categorylist as $list1) {
					if($list["category"] == $list1['category_id']){
						$catname = $list1['category_name'];
					}
				}
				foreach($cuslist as $list2) {
					if($list["customer"]==$list2['id']){
						$cusname = $list2['name'];
					}
				}
                $objPHPExcel->getActiveSheet()->getCell('B'.$s)->setValue($i);
                $objPHPExcel->getActiveSheet()->getCell('C'.$s)->setValue($list['bill']);
                $objPHPExcel->getActiveSheet()->getCell('D'.$s)->setValue($catname); 
                $objPHPExcel->getActiveSheet()->getCell('E'.$s)->setValue($cusname);
                $objPHPExcel->getActiveSheet()->getCell('F'.$s)->setValue(PHPExcel_Shared_Date::PHPToExcel(strtotime($list['date'])));
                $objPHPExcel->getActiveSheet()->getStyle('F'.$s)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);
				$objPHPExcel->getActiveSheet()->getCell('G'.$s)->setValue($list['price']);
				$objPHPExcel->getActiveSheet()->getCell('H'.$s)->setValue($list['quantity']);
				$objPHPExcel->getActiveSheet()->getCell('I'.$s)->setValue($list['tprice']);
				$objPHPExcel->getActiveSheet()->getCell('J'.$s)->setValue($list['amount']);
				$objPHPExcel->getActiveSheet()->getCell('K'.$s)->setValue($list['balance']);
             $s++;
             $i++;
			$totquantity=$totquantity+$list['quantity'];
			$totprice=$totprice+$list['tprice'];
			$totamount=$totamount+$list['amount'];
			$balance=$balance+$list['balance'];
		}
			$objPHPExcel->getActiveSheet()->getCell('G'.$s)->setValue('Total');
			$objPHPExcel->getActiveSheet()->getCell('H'.$s)->setValue($totquantity);
			$objPHPExcel->getActiveSheet()->getCell('I'.$s)->setValue($totprice);
			$objPHPExcel->getActiveSheet()->getCell('J'.$s)->setValue($totamount);
			$objPHPExcel->getActiveSheet()->getCell('K'.$s)->setValue($balance);
			$objPHPExcel->getActiveSheet()->getStyle('G'.$s.':K'.$s)->applyFromArray($smalltitle_style);
		} if(($data=='purchaseitem') || ($data =='purchasevenitem') || ($data =='superpurchase')) {
		if($data=='purchaseitem') {
			$CI->load->model('purchaseadmin/Searchcustomer_Model', 'purchase');
		} else if($data =='purchasevenitem') {
			$CI->load->model('purchaseadmin/Searchreport_Model', 'purchase');
		} else if($data =='superpurchase') {
			$CI->load->model('superadmin/Purchase_Model', 'purchase');
		}
		$srchitemlist = $CI->purchase->srchitemlist($srch);	
		$categorylist = $CI->purchase->categorylist();
		$venlist = $CI->purchase->venlist();
		
		$countRows = count($srchitemlist);
		
        $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B2:J2');
        $objPHPExcel->getActiveSheet()->getCell('B2')->setValue('Purchase Item Details');  
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D3:E3');
        $objPHPExcel->getActiveSheet()->getCell('D3')->setValue('Total no of Entries');
        $objPHPExcel->getActiveSheet()->getCell('F3')->setValue($countRows);   
        $objPHPExcel->getActiveSheet()->getCell('B5')->setValue('SI Number');
        $objPHPExcel->getActiveSheet()->getCell('C5')->setValue('Bill');
        $objPHPExcel->getActiveSheet()->getCell('D5')->setValue('Category');
		$objPHPExcel->getActiveSheet()->getCell('E5')->setValue('Vendor Name');
        $objPHPExcel->getActiveSheet()->getCell('F5')->setValue('Date');
		$objPHPExcel->getActiveSheet()->getCell('G5')->setValue('Description');
		$objPHPExcel->getActiveSheet()->getCell('H5')->setValue('Price');
		$objPHPExcel->getActiveSheet()->getCell('I5')->setValue('Quantity');
		$objPHPExcel->getActiveSheet()->getCell('J5')->setValue('Total Price');
		$objPHPExcel->getActiveSheet()->getCell('K5')->setValue('Received Payment');
		$objPHPExcel->getActiveSheet()->getCell('L5')->setValue('Balance');
        
        $objPHPExcel->getActiveSheet()->getStyle('B5:C5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('D5:E5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('F5:G5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('H5:I5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('J5:K5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($main_title_style);
                
        $s          = 6;
        $i          = 1;
		$totquantity=0;
		$totprice=0;
		$totamount=0;
		$balance=0;
		foreach ($srchitemlist as $list) {
				$catname = '';
				$cusname = '';
				foreach($categorylist as $list1) { 
										if($list["category"]==$list1['id']){
										$catname = $list1['catname'];
										}
									}
				foreach($venlist as $list2) { 
										if($list["vendor"]==$list2['id']){
										$cusname = $list2['name'];
										}
									}
                $objPHPExcel->getActiveSheet()->getCell('B'.$s)->setValue($i);
                $objPHPExcel->getActiveSheet()->getCell('C'.$s)->setValue($list['bill']);
                $objPHPExcel->getActiveSheet()->getCell('D'.$s)->setValue($catname); 
                $objPHPExcel->getActiveSheet()->getCell('E'.$s)->setValue($cusname);
                $objPHPExcel->getActiveSheet()->getCell('F'.$s)->setValue(PHPExcel_Shared_Date::PHPToExcel(strtotime($list['date'])));
                $objPHPExcel->getActiveSheet()->getStyle('F'.$s)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);
				$objPHPExcel->getActiveSheet()->getCell('G'.$s)->setValue($list['description']);
				$objPHPExcel->getActiveSheet()->getCell('H'.$s)->setValue($list['price']);
				$objPHPExcel->getActiveSheet()->getCell('I'.$s)->setValue($list['quantity']);
				$objPHPExcel->getActiveSheet()->getCell('J'.$s)->setValue($list['totprice']);
				$objPHPExcel->getActiveSheet()->getCell('K'.$s)->setValue($list['amount']);
				$objPHPExcel->getActiveSheet()->getCell('L'.$s)->setValue($list['balance']);
             $s++;
             $i++;
			 $totquantity=$totquantity+$list['quantity'];
			$totprice=$totprice+$list['totprice'];
			$totamount=$totamount+$list['amount'];
			$balance=$balance+$list['balance'];
		}
			$objPHPExcel->getActiveSheet()->getCell('H'.$s)->setValue('Total');
			$objPHPExcel->getActiveSheet()->getCell('I'.$s)->setValue($totquantity);
			$objPHPExcel->getActiveSheet()->getCell('J'.$s)->setValue($totprice);
			$objPHPExcel->getActiveSheet()->getCell('K'.$s)->setValue($totamount);
			$objPHPExcel->getActiveSheet()->getCell('L'.$s)->setValue($balance);
			$objPHPExcel->getActiveSheet()->getStyle('H'.$s.':L'.$s)->applyFromArray($smalltitle_style);
		} 
		if($data=='maintenance') {
		
			$CI->load->model('purchaseadmin/Maintenance_Model', 'maintenance');
		
		
		$srchitemlist = $CI->maintenance->mainlist();	
		$epmtlist = $CI->maintenance->epmtlist();

		
		$countRows = count($srchitemlist);
		
        $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B2:J2');
        $objPHPExcel->getActiveSheet()->getCell('B2')->setValue('Maintenance Details');  
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D3:E3');
        $objPHPExcel->getActiveSheet()->getCell('D3')->setValue('Total no of Entries');
        $objPHPExcel->getActiveSheet()->getCell('F3')->setValue($countRows);   
        $objPHPExcel->getActiveSheet()->getCell('B5')->setValue('SI Number');
        $objPHPExcel->getActiveSheet()->getCell('C5')->setValue('Equipment Name');
        $objPHPExcel->getActiveSheet()->getCell('D5')->setValue('ID');
		 $objPHPExcel->getActiveSheet()->getCell('E5')->setValue('Cost');
        $objPHPExcel->getActiveSheet()->getCell('F5')->setValue('Problem description');
		$objPHPExcel->getActiveSheet()->getCell('G5')->setValue('Date');
		$objPHPExcel->getActiveSheet()->getCell('H5')->setValue('Equipment Status');
		$objPHPExcel->getActiveSheet()->getCell('I5')->setValue('Note');
		//$objPHPExcel->getActiveSheet()->getCell('J5')->setValue('Note');
        
        $objPHPExcel->getActiveSheet()->getStyle('B5:C5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('D5:E5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('F5:G5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('H5:I5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('J5:K5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($main_title_style);
                
        $s          = 6;
        $i          = 1;

		$cost=0;

		foreach ($srchitemlist as $list) {
				foreach($epmtlist as $list1) { 
					if($list1['id']==$list['emp_id']){
						$emtname = $list1['name'];
					}
					}
					if($list['status']==0) { $sts = 'In Progress'; } else if($list['status']==1) { $sts = 'Completed'; }
                $objPHPExcel->getActiveSheet()->getCell('B'.$s)->setValue($i);
                $objPHPExcel->getActiveSheet()->getCell('C'.$s)->setValue($emtname);
                $objPHPExcel->getActiveSheet()->getCell('D'.$s)->setValue($list['id']); 
                $objPHPExcel->getActiveSheet()->getCell('E'.$s)->setValue($list['cost']);
                $objPHPExcel->getActiveSheet()->getCell('F'.$s)->setValue($list['problem']);
				$objPHPExcel->getActiveSheet()->getCell('G'.$s)->setValue(PHPExcel_Shared_Date::PHPToExcel(strtotime($list['date'])));
                $objPHPExcel->getActiveSheet()->getStyle('G'.$s)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);
				$objPHPExcel->getActiveSheet()->getCell('H'.$s)->setValue($sts);
				$objPHPExcel->getActiveSheet()->getCell('I'.$s)->setValue($list['notes']);
				//$objPHPExcel->getActiveSheet()->getCell('J'.$s)->setValue($list['amount']);
             $s++;
             $i++;
			 $cost=$cost+$list['cost'];
		
		}
			$objPHPExcel->getActiveSheet()->getCell('D'.$s)->setValue('Total');
			$objPHPExcel->getActiveSheet()->getCell('E'.$s)->setValue($cost);

			$objPHPExcel->getActiveSheet()->getStyle('D'.$s.':E'.$s)->applyFromArray($smalltitle_style);
		}
		if($data=='employee') {
			
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true); 
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true); 
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true); 
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true); 
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
		
		$CI->load->model('operationadmin/Employee_Model', 'employee');
		$emplist = $CI->employee->emplist();	

		
		$countRows = count($emplist);
		
        $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B2:O2');
        $objPHPExcel->getActiveSheet()->getCell('B2')->setValue('Employee Details');  
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D3:E3');
        $objPHPExcel->getActiveSheet()->getCell('D3')->setValue('Total no of Entries');
        $objPHPExcel->getActiveSheet()->getCell('F3')->setValue($countRows);   
        $objPHPExcel->getActiveSheet()->getCell('B5')->setValue('SI Number');
        $objPHPExcel->getActiveSheet()->getCell('C5')->setValue('Name');
		
		 $objPHPExcel->getActiveSheet()->getCell('D5')->setValue('Nationality');
		 $objPHPExcel->getActiveSheet()->getCell('E5')->setValue('National ID');
		 $objPHPExcel->getActiveSheet()->getCell('F5')->setValue('Hire Date');
		 
        $objPHPExcel->getActiveSheet()->getCell('G5')->setValue('Email');
		 $objPHPExcel->getActiveSheet()->getCell('H5')->setValue('Mobile');
        $objPHPExcel->getActiveSheet()->getCell('I5')->setValue('ID No');
		$objPHPExcel->getActiveSheet()->getCell('J5')->setValue('Salary');
		$objPHPExcel->getActiveSheet()->getCell('K5')->setValue('Job');
		
		$objPHPExcel->getActiveSheet()->getCell('L5')->setValue('Notes');
		$objPHPExcel->getActiveSheet()->getCell('M5')->setValue('Bank Name');
		$objPHPExcel->getActiveSheet()->getCell('N5')->setValue('IBAN Number');
		
		
		$objPHPExcel->getActiveSheet()->getCell('O5')->setValue('Status');
		//$objPHPExcel->getActiveSheet()->getCell('J5')->setValue('Note');
        
        $objPHPExcel->getActiveSheet()->getStyle('B5:C5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('D5:E5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('F5:G5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('H5:I5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('J5:K5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($main_title_style);
                
        $s          = 6;
        $i          = 1;

		

		foreach ($emplist as $list) {
				
				if($list['status']==0) { $sts = 'Active'; } else if($list['status']==1) { $sts = 'Vacation'; } else if($list['status']==2) { $sts = 'Quit'; }
                $objPHPExcel->getActiveSheet()->getCell('B'.$s)->setValue($i);
                $objPHPExcel->getActiveSheet()->getCell('C'.$s)->setValue($list['name']);
				
				$objPHPExcel->getActiveSheet()->getCell('D'.$s)->setValue($list['nationality']);
				$objPHPExcel->getActiveSheet()->getCell('E'.$s)->setValue($list['nationid']);
				$objPHPExcel->getActiveSheet()->getCell('F'.$s)->setValue($list['hiredate']);
				
                $objPHPExcel->getActiveSheet()->getCell('G'.$s)->setValue($list['email']); 
                $objPHPExcel->getActiveSheet()->getCell('H'.$s)->setValue($list['mobileno']);
                $objPHPExcel->getActiveSheet()->getCell('I'.$s)->setValue($list['idcard']);
				$objPHPExcel->getActiveSheet()->getCell('J'.$s)->setValue($list['salary']);
				$objPHPExcel->getActiveSheet()->getCell('K'.$s)->setValue($list['job']);
				
				$objPHPExcel->getActiveSheet()->getCell('L'.$s)->setValue($list['notes']);
				$objPHPExcel->getActiveSheet()->getCell('M'.$s)->setValue($list['bankname']);
				$objPHPExcel->getActiveSheet()->getCell('N'.$s)->setValue($list['iban']);
				
				$objPHPExcel->getActiveSheet()->getCell('O'.$s)->setValue($sts);
				
				//$objPHPExcel->getActiveSheet()->getCell('J'.$s)->setValue($list['amount']);
             $s++;
             $i++;
			 
		
		}
			
		}
		if($data=='managecustomer') {
		
			$CI->load->model('salesadmin/Customer_model', 'customer');
		
		
		$cuslist = $CI->customer->cuslist();	
		
		$countRows = count($cuslist);
		
        $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B2:I2');
        $objPHPExcel->getActiveSheet()->getCell('B2')->setValue('Customer Details');  
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('F3:G3');
        $objPHPExcel->getActiveSheet()->getCell('F3')->setValue('Total no of Entries');
        $objPHPExcel->getActiveSheet()->getCell('H3')->setValue($countRows);   
        $objPHPExcel->getActiveSheet()->getCell('B5')->setValue('SI Number');
        $objPHPExcel->getActiveSheet()->getCell('C5')->setValue('Customer ID');
        $objPHPExcel->getActiveSheet()->getCell('D5')->setValue('Name');
		 $objPHPExcel->getActiveSheet()->getCell('E5')->setValue('City');
        $objPHPExcel->getActiveSheet()->getCell('F5')->setValue('Email');
		$objPHPExcel->getActiveSheet()->getCell('G5')->setValue('Address');
		$objPHPExcel->getActiveSheet()->getCell('H5')->setValue('Mobile Number');
		$objPHPExcel->getActiveSheet()->getCell('I5')->setValue('Notes');
        
        $objPHPExcel->getActiveSheet()->getStyle('B5:C5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('D5:E5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('F5:G5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('H5:I5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('J5:K5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($main_title_style);
                
        $s          = 6;
        $i          = 1;


		foreach ($cuslist as $list) {
				
				
                $objPHPExcel->getActiveSheet()->getCell('B'.$s)->setValue($i);
                $objPHPExcel->getActiveSheet()->getCell('C'.$s)->setValue($list['id']);
                $objPHPExcel->getActiveSheet()->getCell('D'.$s)->setValue($list['name']); 
                $objPHPExcel->getActiveSheet()->getCell('E'.$s)->setValue($list['city']);
                $objPHPExcel->getActiveSheet()->getCell('F'.$s)->setValue($list['email']);
				$objPHPExcel->getActiveSheet()->getCell('G'.$s)->setValue($list['address']);
				$objPHPExcel->getActiveSheet()->getCell('H'.$s)->setValue($list['phone']);
				$objPHPExcel->getActiveSheet()->getCell('I'.$s)->setValue($list['notes']);
             $s++;
             $i++;

		
		}
		}
		
		if($data == 'payroll') {

		$CI->load->model('operationadmin/Payment_Model', 'payment');
		$paylist = $CI->payment->paylist();
		$amtlist = $CI->payment->payamtlist();
		
		$countRows = count($paylist);
		
        $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B2:E2');
        $objPHPExcel->getActiveSheet()->getCell('B2')->setValue('Employee Payroll Details');  
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('C3:D3');
        $objPHPExcel->getActiveSheet()->getCell('C3')->setValue('Total no of Entries');
        $objPHPExcel->getActiveSheet()->getCell('E3')->setValue($countRows);   
        $objPHPExcel->getActiveSheet()->getCell('B5')->setValue('S.No');
        $objPHPExcel->getActiveSheet()->getCell('C5')->setValue('Payroll Name');
        $objPHPExcel->getActiveSheet()->getCell('D5')->setValue('Payroll Date');
        $objPHPExcel->getActiveSheet()->getCell('E5')->setValue('Payroll Amount');


        
        $objPHPExcel->getActiveSheet()->getStyle('B5:C5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('D5:E5')->applyFromArray($smalltitle_style);
       
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($main_title_style);
                
        $s          = 6;
        $i          = 1;
		foreach ($paylist as $list) {
				foreach($amtlist as $list1) { 
										if($list['id']==$list1['pay_id']){
										$pay = $list1['total'].' ';
										}
									} 
                $objPHPExcel->getActiveSheet()->getCell('B'.$s)->setValue($i);
                $objPHPExcel->getActiveSheet()->getCell('C'.$s)->setValue($list['name']);
                $objPHPExcel->getActiveSheet()->getCell('D'.$s)->setValue(PHPExcel_Shared_Date::PHPToExcel(strtotime($list['date'])));
                $objPHPExcel->getActiveSheet()->getStyle('D'.$s)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);
                $objPHPExcel->getActiveSheet()->getCell('E'.$s)->setValue($pay);
				
             $s++;
             $i++;
		}
		}
		if($data == 'searchpdnrpt') {

		$CI->load->model('operationadmin/Productionreport_Model', 'pdnrpt');
		$srchitemlist = $CI->pdnrpt->srchitemlist($srch);	
		$terlist = $CI->pdnrpt->terlist();
		$cyclist = $CI->pdnrpt->cyclist();
		$typelist = $CI->pdnrpt->getitemtype();
		
		$countRows = count($srchitemlist);
		
        $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B2:G2');
        $objPHPExcel->getActiveSheet()->getCell('B2')->setValue('Production Report Details');  
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D3:E3');
        $objPHPExcel->getActiveSheet()->getCell('D3')->setValue('Total no of Entries');
        $objPHPExcel->getActiveSheet()->getCell('F3')->setValue($countRows);   
        $objPHPExcel->getActiveSheet()->getCell('B5')->setValue('SI Number');
        $objPHPExcel->getActiveSheet()->getCell('C5')->setValue('Terminal Name');
        $objPHPExcel->getActiveSheet()->getCell('D5')->setValue('Cycle Name');
        $objPHPExcel->getActiveSheet()->getCell('E5')->setValue('Date');
		$objPHPExcel->getActiveSheet()->getCell('F5')->setValue('Item Type');
		$objPHPExcel->getActiveSheet()->getCell('G5')->setValue('Quantity');

        
        $objPHPExcel->getActiveSheet()->getStyle('B5:C5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('D5:E5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('F5:G5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('H5:I5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('J5:K5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($main_title_style);
                
        $s          = 6;
        $i          = 1;
		foreach ($srchitemlist as $list) {				
				$cyclename='';
				$catname = '';
				foreach($terlist as $list1) {				
					if($list['terminalname'] == $list1['id']){
						$tername = $list1['title']; }	
					}
					$dd = explode(',',$list['cyclename']); 
										foreach($dd as $item) {
									foreach($cyclist as $list1) {
									
								if($list1['id'] == $item) {
									$cyclename.=$list1['name'].',';
								}
									} }
					$cycname = trim($cyclename,','); 	
				/*foreach($cyclist as $list1) {				
					if($list['cyclename'] == $list1['id']){
						$cycname = $list1['name']; }	
					}	*/					
				foreach($typelist['catlist'] as $list1) {				
					if($list['itemtype'] == $list1['category_id']){
						$catname = $list1['category_name']; }	
					}
                $objPHPExcel->getActiveSheet()->getCell('B'.$s)->setValue($i);
                $objPHPExcel->getActiveSheet()->getCell('C'.$s)->setValue($tername);
                $objPHPExcel->getActiveSheet()->getCell('D'.$s)->setValue($cycname); 
                $objPHPExcel->getActiveSheet()->getCell('E'.$s)->setValue(PHPExcel_Shared_Date::PHPToExcel(strtotime($list['date'])));
                $objPHPExcel->getActiveSheet()->getStyle('E'.$s)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);
                $objPHPExcel->getActiveSheet()->getCell('F'.$s)->setValue($catname);
				$objPHPExcel->getActiveSheet()->getCell('G'.$s)->setValue($list['quantity']);
             $s++;
             $i++;
			 $cyclename='';
		}
		}
		if($data == 'vendordetails') {

		$CI->load->model('purchaseadmin/Vendor_Model', 'vendor');
		$venlist = $CI->vendor->venlist();

		$countRows = count($venlist);
		
        $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B2:G2');
        $objPHPExcel->getActiveSheet()->getCell('B2')->setValue('Vendor Details');  
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D3:E3');
        $objPHPExcel->getActiveSheet()->getCell('D3')->setValue('Total no of Entries');
        $objPHPExcel->getActiveSheet()->getCell('F3')->setValue($countRows);   
        $objPHPExcel->getActiveSheet()->getCell('B5')->setValue('Vendor ID');
        $objPHPExcel->getActiveSheet()->getCell('C5')->setValue('Name');
        $objPHPExcel->getActiveSheet()->getCell('D5')->setValue('Email');
        $objPHPExcel->getActiveSheet()->getCell('E5')->setValue('City');
		$objPHPExcel->getActiveSheet()->getCell('F5')->setValue('Mobile No');
		$objPHPExcel->getActiveSheet()->getCell('G5')->setValue('Address');

        
        $objPHPExcel->getActiveSheet()->getStyle('B5:C5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('D5:E5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('F5:G5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('H5:I5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('J5:K5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($main_title_style);
                
        $s          = 6;
        $i          = 1;
		foreach ($venlist as $list) {
				
                $objPHPExcel->getActiveSheet()->getCell('B'.$s)->setValue($list['id']);
                $objPHPExcel->getActiveSheet()->getCell('C'.$s)->setValue($list['name']);
                $objPHPExcel->getActiveSheet()->getCell('D'.$s)->setValue($list['email']); 
                $objPHPExcel->getActiveSheet()->getCell('E'.$s)->setValue($list['city']);
                $objPHPExcel->getActiveSheet()->getCell('F'.$s)->setValue($list['phone']);
				$objPHPExcel->getActiveSheet()->getCell('G'.$s)->setValue($list['address']);
             $s++;
             $i++;
		}
		}
		if($data == 'searchcategory') {

		$CI->load->model('salesadmin/Searchcategory_Model', 'category');
		$categorylist = $CI->category->categorylist();

		$countRows = count($categorylist);
		
        $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B2:C2');
        $objPHPExcel->getActiveSheet()->getCell('B2')->setValue('Item Type Details');  
        //$objPHPExcel->setActiveSheetIndex(0)->mergeCells('D3:C3');
        $objPHPExcel->getActiveSheet()->getCell('B3')->setValue('Total no of Entries');
        $objPHPExcel->getActiveSheet()->getCell('C3')->setValue($countRows);   
        $objPHPExcel->getActiveSheet()->getCell('B5')->setValue('Item ID');
        $objPHPExcel->getActiveSheet()->getCell('C5')->setValue('Item Type');

        
        $objPHPExcel->getActiveSheet()->getStyle('B5:C5')->applyFromArray($smalltitle_style);
        
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($main_title_style);
                
        $s          = 6;
        $i          = 1;
		foreach ($categorylist['catlist'] as $list) {
				
                $objPHPExcel->getActiveSheet()->getCell('B'.$s)->setValue($list['category_id']);
                $objPHPExcel->getActiveSheet()->getCell('C'.$s)->setValue($list['category_name']);
               
             $s++;
             $i++;
		}
		}
        if($data == 'purchasecategory') {

		$CI->load->model('purchaseadmin/Category_Model', 'category');
		$categorylist = $CI->category->categorylist();

		$countRows = count($categorylist);
		
        $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B2:C2');
        $objPHPExcel->getActiveSheet()->getCell('B2')->setValue('Category Details');  
        //$objPHPExcel->setActiveSheetIndex(0)->mergeCells('D3:C3');
        $objPHPExcel->getActiveSheet()->getCell('B3')->setValue('Total no of Entries');
        $objPHPExcel->getActiveSheet()->getCell('C3')->setValue($countRows);   
        $objPHPExcel->getActiveSheet()->getCell('B5')->setValue('Category ID');
        $objPHPExcel->getActiveSheet()->getCell('C5')->setValue('Category');

        
        $objPHPExcel->getActiveSheet()->getStyle('B5:C5')->applyFromArray($smalltitle_style);
        
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($main_title_style);
                
        $s          = 6;
        $i          = 1;
		foreach ($categorylist as $list) {
				
                $objPHPExcel->getActiveSheet()->getCell('B'.$s)->setValue($list['id']);
                $objPHPExcel->getActiveSheet()->getCell('C'.$s)->setValue($list['catname']);
               
             $s++;
             $i++;
		}
		}
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
		// Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="OrderReport-'.date('Ymd h_i_s ').'.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;

    }
	function xlsPrintOrder_emppay($value,$title){
		$count=count($value);
		$CI     = get_instance();
        
        $ORDNO       = '#'.str_pad(1, 6, '0', STR_PAD_LEFT);

        // Create new PHPExcel object
        $objPHPExcel            = new PHPExcel();
       // $orderProductsLists     = $CI->Settingmodel->orderProductsList($orderID);
        //$orderInfo              = $CI->Settingmodel->orderInfo($orderID);
        //$countRows              = count($orderProductsLists);

        // $Gtotal                 = $orderInfo->total;
        // $shippingPrice  = '';
        // $shippingPrice1 = 0;
        
        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                     ->setLastModifiedBy("Maarten Balliauw")
                                     ->setTitle("Office 2007 XLSX Test Document")
                                     ->setSubject("Office 2007 XLSX Test Document")
                                     ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                     ->setKeywords("office 2007 openxml php")
                                     ->setCategory("Test result file");


                $main_title_style = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => '31849b'),
                        'size'  => 20,
                        
                    ));
                $smalltitle_style = array(
                    'font'  => array(
                        'bold'  => true,
                        'size'  => 12
                    ),
                    'alignment' => array(
                            'wrap'       => true
                    )
                );
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true); 
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true); 
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
        //////////////////////////////////
        
        $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B2:H2');
        $objPHPExcel->getActiveSheet()->getCell('B2')->setValue($title);  
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('F3:G3');
        $objPHPExcel->getActiveSheet()->getCell('F3')->setValue('Total no of Entries');
        $objPHPExcel->getActiveSheet()->getCell('H3')->setValue($count);   
        $objPHPExcel->getActiveSheet()->getCell('B5')->setValue('S.No');
        $objPHPExcel->getActiveSheet()->getCell('C5')->setValue('Payroll Name');
        $objPHPExcel->getActiveSheet()->getCell('D5')->setValue('Employee Name');
		
		$objPHPExcel->getActiveSheet()->getCell('E5')->setValue('Nationality');
		$objPHPExcel->getActiveSheet()->getCell('F5')->setValue('Nationid');
		$objPHPExcel->getActiveSheet()->getCell('G5')->setValue('Mobile No');
		$objPHPExcel->getActiveSheet()->getCell('H5')->setValue('Email ID');
		$objPHPExcel->getActiveSheet()->getCell('I5')->setValue('Idcard');
		$objPHPExcel->getActiveSheet()->getCell('J5')->setValue('Job');
		$objPHPExcel->getActiveSheet()->getCell('K5')->setValue('Bank Name');
		$objPHPExcel->getActiveSheet()->getCell('L5')->setValue('IBAN');

        $objPHPExcel->getActiveSheet()->getCell('M5')->setValue('Extra Amount');
        $objPHPExcel->getActiveSheet()->getCell('N5')->setValue('Deduction');
		$objPHPExcel->getActiveSheet()->getCell('O5')->setValue('Salary Amount');
		$objPHPExcel->getActiveSheet()->getCell('P5')->setValue('Payroll Amount');
        
        $objPHPExcel->getActiveSheet()->getStyle('B5:C5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('D5:E5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('F5:G5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('H5:I5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('J5:K5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($main_title_style);
                
        $s          = 6;        
        $i          = 1;
		$total		= 0;
		$paytotal	= 0;
foreach ($value as $list) {
		$objPHPExcel->getActiveSheet()->getCell('B'.$s)->setValue($i);
		$objPHPExcel->getActiveSheet()->getCell('C'.$s)->setValue($list['payname']);
		$objPHPExcel->getActiveSheet()->getCell('D'.$s)->setValue($list['empname']); 
		
		$objPHPExcel->getActiveSheet()->getCell('E'.$s)->setValue($list['nationality']);
		$objPHPExcel->getActiveSheet()->getCell('F'.$s)->setValue($list['nationid']);
		$objPHPExcel->getActiveSheet()->getCell('G'.$s)->setValue($list['mobileno']);
		$objPHPExcel->getActiveSheet()->getCell('H'.$s)->setValue($list['email']);
		$objPHPExcel->getActiveSheet()->getCell('I'.$s)->setValue($list['idcard']);
		$objPHPExcel->getActiveSheet()->getCell('J'.$s)->setValue($list['job']);
		$objPHPExcel->getActiveSheet()->getCell('K'.$s)->setValue($list['bankname']);
		$objPHPExcel->getActiveSheet()->getCell('L'.$s)->setValue($list['iban']);
						
		$objPHPExcel->getActiveSheet()->getCell('M'.$s)->setValue($list['extra_amt']);
		$objPHPExcel->getActiveSheet()->getCell('N'.$s)->setValue($list['deduct_amt']);
		$objPHPExcel->getActiveSheet()->getCell('O'.$s)->setValue($list['total']);
		$objPHPExcel->getActiveSheet()->getCell('P'.$s)->setValue($list['paytotal']);
             $s++;
             $i++;
			  $total=$total+$list['total'];
			  $paytotal=$paytotal+$list['paytotal'];
        }
       
		$objPHPExcel->getActiveSheet()->getCell('N'.$s)->setValue('Total');
		$objPHPExcel->getActiveSheet()->getCell('O'.$s)->setValue($total);
		$objPHPExcel->getActiveSheet()->getCell('P'.$s)->setValue($paytotal);
		$objPHPExcel->getActiveSheet()->getStyle('N'.$s.':P'.$s)->applyFromArray($smalltitle_style);
		
        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$countRow1.':E'.$countRow1);
        // $objPHPExcel->getActiveSheet()->getCell('D'.$countRow1)->setValue('Shipping');
        // $objPHPExcel->getActiveSheet()->getCell('F'.$countRow1)->setValue($shippingPrice);

        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$countRow2 .':E'.$countRow2);
        // $objPHPExcel->getActiveSheet()->getCell('D'.$countRow2)->setValue('Grand Total');
        // $objPHPExcel->getActiveSheet()->getCell('F'.$countRow2)->setValue($Gtotal);
            
             //////////////////////////////////////////////
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
// Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="OrderReport-'.date('Ymd h_i_s ').'.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
	}
	function xlsPrintOrder_consumption($value,$title){
		$count=count($value);
		$CI     = get_instance();
        
        $ORDNO       = '#'.str_pad(1, 6, '0', STR_PAD_LEFT);

        // Create new PHPExcel object
        $objPHPExcel            = new PHPExcel();
       // $orderProductsLists     = $CI->Settingmodel->orderProductsList($orderID);
        //$orderInfo              = $CI->Settingmodel->orderInfo($orderID);
        //$countRows              = count($orderProductsLists);

        // $Gtotal                 = $orderInfo->total;
        // $shippingPrice  = '';
        // $shippingPrice1 = 0;
        
        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                     ->setLastModifiedBy("Maarten Balliauw")
                                     ->setTitle("Office 2007 XLSX Test Document")
                                     ->setSubject("Office 2007 XLSX Test Document")
                                     ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                     ->setKeywords("office 2007 openxml php")
                                     ->setCategory("Test result file");


                $main_title_style = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => '31849b'),
                        'size'  => 20,
                        
                    ));
                $smalltitle_style = array(
                    'font'  => array(
                        'bold'  => true,
                        'size'  => 12
                    ),
                    'alignment' => array(
                            'wrap'       => true
                    )
                );
 $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);  
		 $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        //////////////////////////////////
        
        $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B2:F2');
        $objPHPExcel->getActiveSheet()->getCell('B2')->setValue($title);  
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D3:E3');
        $objPHPExcel->getActiveSheet()->getCell('D3')->setValue('Total no of Entries');
        $objPHPExcel->getActiveSheet()->getCell('F3')->setValue($count);   
        $objPHPExcel->getActiveSheet()->getCell('B5')->setValue('S.No');
        $objPHPExcel->getActiveSheet()->getCell('C5')->setValue('Terminal Name');
        $objPHPExcel->getActiveSheet()->getCell('D5')->setValue('Cycle Name');
        $objPHPExcel->getActiveSheet()->getCell('E5')->setValue('Category');
		$objPHPExcel->getActiveSheet()->getCell('F5')->setValue('Date');
        $objPHPExcel->getActiveSheet()->getCell('G5')->setValue('Quantity');
        
        $objPHPExcel->getActiveSheet()->getStyle('B5:C5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('D5:E5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('F5:G5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('H5:I5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('J5:K5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($main_title_style);
                
        $s          = 6;        
        $i          = 1;
		$quantity	= 0;
foreach ($value as $list) {
                $objPHPExcel->getActiveSheet()->getCell('B'.$s)->setValue($i);
                $objPHPExcel->getActiveSheet()->getCell('C'.$s)->setValue($list['terminalname']);
                $objPHPExcel->getActiveSheet()->getCell('D'.$s)->setValue($list['cyclename']); 
                $objPHPExcel->getActiveSheet()->getCell('E'.$s)->setValue($list['category']);
				$objPHPExcel->getActiveSheet()->getCell('F'.$s)->setValue(PHPExcel_Shared_Date::PHPToExcel(strtotime($list['date'])));
                $objPHPExcel->getActiveSheet()->getStyle('F'.$s)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);
                $objPHPExcel->getActiveSheet()->getCell('G'.$s)->setValue($list['quantity']);
             $s++;
             $i++;
			 $quantity=$quantity+$list['quantity'];
        }
	
		$objPHPExcel->getActiveSheet()->getCell('F'.$s)->setValue('Total');
		$objPHPExcel->getActiveSheet()->getCell('G'.$s)->setValue($quantity);
		$objPHPExcel->getActiveSheet()->getStyle('F'.$s.':G'.$s)->applyFromArray($smalltitle_style);
        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$countRow.':E'.$countRow);
        // $objPHPExcel->getActiveSheet()->getCell('D'.$countRow)->setValue('Total');
        // $objPHPExcel->getActiveSheet()->getCell('F'.$countRow)->setValue($Stotal);

        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$countRow1.':E'.$countRow1);
        // $objPHPExcel->getActiveSheet()->getCell('D'.$countRow1)->setValue('Shipping');
        // $objPHPExcel->getActiveSheet()->getCell('F'.$countRow1)->setValue($shippingPrice);

        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$countRow2 .':E'.$countRow2);
        // $objPHPExcel->getActiveSheet()->getCell('D'.$countRow2)->setValue('Grand Total');
        // $objPHPExcel->getActiveSheet()->getCell('F'.$countRow2)->setValue($Gtotal);
            
             //////////////////////////////////////////////
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
// Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="OrderReport-'.date('Ymd h_i_s ').'.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
	}
	function xlsPrintOrder_paylist($value,$title){
		$count=count($value);
		$CI     = get_instance();
        
        $ORDNO       = '#'.str_pad(1, 6, '0', STR_PAD_LEFT);

        // Create new PHPExcel object
        $objPHPExcel            = new PHPExcel();
       // $orderProductsLists     = $CI->Settingmodel->orderProductsList($orderID);
        //$orderInfo              = $CI->Settingmodel->orderInfo($orderID);
        //$countRows              = count($orderProductsLists);

        // $Gtotal                 = $orderInfo->total;
        // $shippingPrice  = '';
        // $shippingPrice1 = 0;
        
        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                     ->setLastModifiedBy("Maarten Balliauw")
                                     ->setTitle("Office 2007 XLSX Test Document")
                                     ->setSubject("Office 2007 XLSX Test Document")
                                     ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                     ->setKeywords("office 2007 openxml php")
                                     ->setCategory("Test result file");


                $main_title_style = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => '31849b'),
                        'size'  => 20,
                        
                    ));
                $smalltitle_style = array(
                    'font'  => array(
                        'bold'  => true,
                        'size'  => 12
                    ),
                    'alignment' => array(
                            'wrap'       => true
                    )
                );
 $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true); 
		 $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true); 
		  
        //////////////////////////////////
        
        $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B2:J2');
        $objPHPExcel->getActiveSheet()->getCell('B2')->setValue($title);  
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('G3:I3');
        $objPHPExcel->getActiveSheet()->getCell('G3')->setValue('Total no of Entries');
        $objPHPExcel->getActiveSheet()->getCell('J3')->setValue($count);   
        $objPHPExcel->getActiveSheet()->getCell('B5')->setValue('S.No');
        $objPHPExcel->getActiveSheet()->getCell('C5')->setValue('Payroll Name');
        $objPHPExcel->getActiveSheet()->getCell('D5')->setValue('Employee Name');        
        $objPHPExcel->getActiveSheet()->getCell('E5')->setValue('Bank Name');
		$objPHPExcel->getActiveSheet()->getCell('F5')->setValue('IBAN #');
		$objPHPExcel->getActiveSheet()->getCell('G5')->setValue('Extra Amount');
		$objPHPExcel->getActiveSheet()->getCell('H5')->setValue('Deduction');
		$objPHPExcel->getActiveSheet()->getCell('I5')->setValue('Salary Amount');
		$objPHPExcel->getActiveSheet()->getCell('J5')->setValue('Paid Amount');

        
        $objPHPExcel->getActiveSheet()->getStyle('B5:C5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('D5:E5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('F5:G5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('H5:I5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('J5:K5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($main_title_style);
                
        $s          = 6;        
        $i          = 1;
		$totsal=0;
		$paytotal=0;
		foreach ($value as $list) {
                $objPHPExcel->getActiveSheet()->getCell('B'.$s)->setValue($i);
                $objPHPExcel->getActiveSheet()->getCell('C'.$s)->setValue($list['payname']);  
				$objPHPExcel->getActiveSheet()->getCell('D'.$s)->setValue($list['empname']);				
                $objPHPExcel->getActiveSheet()->getCell('E'.$s)->setValue($list['bank']);
				$objPHPExcel->getActiveSheet()->getCell('F'.$s)->setValue($list['iban']);
				$objPHPExcel->getActiveSheet()->getCell('G'.$s)->setValue($list['extra_amt']);
				$objPHPExcel->getActiveSheet()->getCell('H'.$s)->setValue($list['deduct_amt']);
				$objPHPExcel->getActiveSheet()->getCell('I'.$s)->setValue($list['salary']);
				$objPHPExcel->getActiveSheet()->getCell('J'.$s)->setValue($list['paytotal']);
				
             $s++;
             $i++;
			 $totsal=$totsal+$list['salary'];
			$paytotal=$paytotal+$list['paytotal'];
        }
		$objPHPExcel->getActiveSheet()->getCell('H'.$s)->setValue('Total');
		$objPHPExcel->getActiveSheet()->getCell('I'.$s)->setValue($totsal);
		$objPHPExcel->getActiveSheet()->getCell('J'.$s)->setValue($paytotal);
		$objPHPExcel->getActiveSheet()->getStyle('H'.$s.':J'.$s)->applyFromArray($smalltitle_style);
        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$countRow.':E'.$countRow);
        // $objPHPExcel->getActiveSheet()->getCell('D'.$countRow)->setValue('Total');
        // $objPHPExcel->getActiveSheet()->getCell('F'.$countRow)->setValue($Stotal);

        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$countRow1.':E'.$countRow1);
        // $objPHPExcel->getActiveSheet()->getCell('D'.$countRow1)->setValue('Shipping');
        // $objPHPExcel->getActiveSheet()->getCell('F'.$countRow1)->setValue($shippingPrice);

        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$countRow2 .':E'.$countRow2);
        // $objPHPExcel->getActiveSheet()->getCell('D'.$countRow2)->setValue('Grand Total');
        // $objPHPExcel->getActiveSheet()->getCell('F'.$countRow2)->setValue($Gtotal);
            
             //////////////////////////////////////////////
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
// Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="OrderReport-'.date('Ymd h_i_s ').'.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
	}	
	function xlsPrintOrder_paylist1($value,$title){
		$count=count($value);
		$CI     = get_instance();
        
        $ORDNO       = '#'.str_pad(1, 6, '0', STR_PAD_LEFT);

        // Create new PHPExcel object
        $objPHPExcel            = new PHPExcel();
       // $orderProductsLists     = $CI->Settingmodel->orderProductsList($orderID);
        //$orderInfo              = $CI->Settingmodel->orderInfo($orderID);
        //$countRows              = count($orderProductsLists);

        // $Gtotal                 = $orderInfo->total;
        // $shippingPrice  = '';
        // $shippingPrice1 = 0;
        
        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                     ->setLastModifiedBy("Maarten Balliauw")
                                     ->setTitle("Office 2007 XLSX Test Document")
                                     ->setSubject("Office 2007 XLSX Test Document")
                                     ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                     ->setKeywords("office 2007 openxml php")
                                     ->setCategory("Test result file");


                $main_title_style = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => '31849b'),
                        'size'  => 20,
                        
                    ));
                $smalltitle_style = array(
                    'font'  => array(
                        'bold'  => true,
                        'size'  => 12
                    ),
                    'alignment' => array(
                            'wrap'       => true
                    )
                );
 $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true); 
		 $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true); 
		 $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        //////////////////////////////////
        
        $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B2:I2');
        $objPHPExcel->getActiveSheet()->getCell('B2')->setValue($title);  
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('F3:G3');
        $objPHPExcel->getActiveSheet()->getCell('F3')->setValue('Total no of Entries');
        $objPHPExcel->getActiveSheet()->getCell('H3')->setValue($count);   
        $objPHPExcel->getActiveSheet()->getCell('B5')->setValue('S.No');
        $objPHPExcel->getActiveSheet()->getCell('C5')->setValue('Payroll Name');
        $objPHPExcel->getActiveSheet()->getCell('D5')->setValue('Employee Name');
		$objPHPExcel->getActiveSheet()->getCell('E5')->setValue('Salary');
        $objPHPExcel->getActiveSheet()->getCell('F5')->setValue('Extra Amount');
        $objPHPExcel->getActiveSheet()->getCell('G5')->setValue('Deduction');
		$objPHPExcel->getActiveSheet()->getCell('H5')->setValue('Total');
		$objPHPExcel->getActiveSheet()->getCell('I5')->setValue('Payroll Amount');
        
        $objPHPExcel->getActiveSheet()->getStyle('B5:C5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('D5:E5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('F5:G5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('H5:I5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('J5:K5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($main_title_style);
                
        $s          = 6;        
        $i          = 1;
foreach ($value as $list) {
                $objPHPExcel->getActiveSheet()->getCell('B'.$s)->setValue($i);
                $objPHPExcel->getActiveSheet()->getCell('C'.$s)->setValue($list['payname']);
                $objPHPExcel->getActiveSheet()->getCell('D'.$s)->setValue($list['empname']); 
				$objPHPExcel->getActiveSheet()->getCell('E'.$s)->setValue($list['salary']);
                $objPHPExcel->getActiveSheet()->getCell('F'.$s)->setValue($list['extra_amt']);
                $objPHPExcel->getActiveSheet()->getCell('G'.$s)->setValue($list['deduct_amt']);
				$objPHPExcel->getActiveSheet()->getCell('H'.$s)->setValue($list['total']);
				$objPHPExcel->getActiveSheet()->getCell('I'.$s)->setValue($list['paytotal']);
             $s++;
             $i++;
        }
        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$countRow.':E'.$countRow);
        // $objPHPExcel->getActiveSheet()->getCell('D'.$countRow)->setValue('Total');
        // $objPHPExcel->getActiveSheet()->getCell('F'.$countRow)->setValue($Stotal);

        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$countRow1.':E'.$countRow1);
        // $objPHPExcel->getActiveSheet()->getCell('D'.$countRow1)->setValue('Shipping');
        // $objPHPExcel->getActiveSheet()->getCell('F'.$countRow1)->setValue($shippingPrice);

        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$countRow2 .':E'.$countRow2);
        // $objPHPExcel->getActiveSheet()->getCell('D'.$countRow2)->setValue('Grand Total');
        // $objPHPExcel->getActiveSheet()->getCell('F'.$countRow2)->setValue($Gtotal);
            
             //////////////////////////////////////////////
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
// Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="OrderReport-'.date('Ymd h_i_s ').'.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
	}
	function xlsPrintOrder_maintenance($value,$title){
		$count=count($value);
		$CI     = get_instance();
        
        $ORDNO       = '#'.str_pad(1, 6, '0', STR_PAD_LEFT);

        // Create new PHPExcel object
        $objPHPExcel            = new PHPExcel();
       // $orderProductsLists     = $CI->Settingmodel->orderProductsList($orderID);
        //$orderInfo              = $CI->Settingmodel->orderInfo($orderID);
        //$countRows              = count($orderProductsLists);

        // $Gtotal                 = $orderInfo->total;
        // $shippingPrice  = '';
        // $shippingPrice1 = 0;
        
        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                     ->setLastModifiedBy("Maarten Balliauw")
                                     ->setTitle("Office 2007 XLSX Test Document")
                                     ->setSubject("Office 2007 XLSX Test Document")
                                     ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                     ->setKeywords("office 2007 openxml php")
                                     ->setCategory("Test result file");


                $main_title_style = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => '31849b'),
                        'size'  => 20,
                        
                    ));
                $smalltitle_style = array(
                    'font'  => array(
                        'bold'  => true,
                        'size'  => 12
                    ),
                    'alignment' => array(
                            'wrap'       => true
                    )
                );
 $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);  
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);  
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);  
        //////////////////////////////////
        
        $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B2:H2');
        $objPHPExcel->getActiveSheet()->getCell('B2')->setValue($title);  
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('F3:G3');
        $objPHPExcel->getActiveSheet()->getCell('F3')->setValue('Total no of Entries');
        $objPHPExcel->getActiveSheet()->getCell('H3')->setValue($count);   
        $objPHPExcel->getActiveSheet()->getCell('B5')->setValue('S.No');
        $objPHPExcel->getActiveSheet()->getCell('C5')->setValue('Equipment Name');
        $objPHPExcel->getActiveSheet()->getCell('D5')->setValue('Problem description');
        $objPHPExcel->getActiveSheet()->getCell('E5')->setValue('Date');
        $objPHPExcel->getActiveSheet()->getCell('F5')->setValue('Equipment Status');
		$objPHPExcel->getActiveSheet()->getCell('G5')->setValue('Note');
		$objPHPExcel->getActiveSheet()->getCell('H5')->setValue('Cost');
        
        $objPHPExcel->getActiveSheet()->getStyle('B5:C5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('D5:E5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('F5:G5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('H5:I5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('J5:K5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($main_title_style);
                
        $s          = 6;        
        $i          = 1;
		$totalcost=0;
foreach ($value as $list) {
	if($list['status']==0) { $sts = 'In Progress'; } else if($list['status']==1) { $sts = 'Completed'; }
                $objPHPExcel->getActiveSheet()->getCell('B'.$s)->setValue($i);
                $objPHPExcel->getActiveSheet()->getCell('C'.$s)->setValue($list['name']); 
                $objPHPExcel->getActiveSheet()->getCell('D'.$s)->setValue($list['problem']);
                $objPHPExcel->getActiveSheet()->getCell('E'.$s)->setValue($list['date']);
				$objPHPExcel->getActiveSheet()->getCell('F'.$s)->setValue($sts);
				$objPHPExcel->getActiveSheet()->getCell('G'.$s)->setValue($list['notes']);
                $objPHPExcel->getActiveSheet()->getCell('H'.$s)->setValue($list['cost']);
             $s++;
             $i++;
			 $totalcost=$totalcost+$list['cost'];
        }
		//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B'.$s.':F'.$s);
		$objPHPExcel->getActiveSheet()->getCell('G'.$s)->setValue('Total');
		$objPHPExcel->getActiveSheet()->getCell('H'.$s)->setValue($totalcost);
		$objPHPExcel->getActiveSheet()->getStyle('G'.$s.':H'.$s)->applyFromArray($smalltitle_style);
        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$countRow.':E'.$countRow);
        // $objPHPExcel->getActiveSheet()->getCell('D'.$countRow)->setValue('Total');
        // $objPHPExcel->getActiveSheet()->getCell('F'.$countRow)->setValue($Stotal);

        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$countRow1.':E'.$countRow1);
        // $objPHPExcel->getActiveSheet()->getCell('D'.$countRow1)->setValue('Shipping');
        // $objPHPExcel->getActiveSheet()->getCell('F'.$countRow1)->setValue($shippingPrice);

        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$countRow2 .':E'.$countRow2);
        // $objPHPExcel->getActiveSheet()->getCell('D'.$countRow2)->setValue('Grand Total');
        // $objPHPExcel->getActiveSheet()->getCell('F'.$countRow2)->setValue($Gtotal);
            
             //////////////////////////////////////////////
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
// Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="OrderReport-'.date('Ymd h_i_s ').'.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
	}
	function xlsPrintOrder_stock($value,$title){
		$CI     = get_instance();
		$CI->load->model('operationadmin/Stock_Model', 'stock');
		$catprice = $CI->stock->catprice();
		$count=count($value);
		$CI     = get_instance();
        
        $ORDNO       = '#'.str_pad(1, 6, '0', STR_PAD_LEFT);

        // Create new PHPExcel object
        $objPHPExcel            = new PHPExcel();
       // $orderProductsLists     = $CI->Settingmodel->orderProductsList($orderID);
        //$orderInfo              = $CI->Settingmodel->orderInfo($orderID);
        //$countRows              = count($orderProductsLists);

        // $Gtotal                 = $orderInfo->total;
        // $shippingPrice  = '';
        // $shippingPrice1 = 0;
        
        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                     ->setLastModifiedBy("Maarten Balliauw")
                                     ->setTitle("Office 2007 XLSX Test Document")
                                     ->setSubject("Office 2007 XLSX Test Document")
                                     ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                     ->setKeywords("office 2007 openxml php")
                                     ->setCategory("Test result file");


                $main_title_style = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => '31849b'),
                        'size'  => 20,
                        
                    ));
                $smalltitle_style = array(
                    'font'  => array(
                        'bold'  => true,
                        'size'  => 12
                    ),
                    'alignment' => array(
                            'wrap'       => true
                    )
                );
 $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); 
         
        //////////////////////////////////
        
        $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B2:C2');
        $objPHPExcel->getActiveSheet()->getCell('B2')->setValue($title);  
        
        $objPHPExcel->getActiveSheet()->getCell('B3')->setValue('Total no of Entries');
        $objPHPExcel->getActiveSheet()->getCell('C3')->setValue($count);   
        $objPHPExcel->getActiveSheet()->getCell('B5')->setValue('Category');
        $objPHPExcel->getActiveSheet()->getCell('C5')->setValue('Stock');
        
        
        $objPHPExcel->getActiveSheet()->getStyle('B5:C5')->applyFromArray($smalltitle_style);
       
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($main_title_style);
                
        $s          = 6;        
        $i          = 1;
		$totalcost=0;
		$cat = 0;
		foreach ($value as $list) {
				foreach($catprice as $list1) { 
					if($list1['category']==$list['id']){
						if($list1['quantity']!=''){ 
							$cat = $list1['quantity']-$list1['qtybal']; 
						} else {
							$cat = 0;
						}
					}
					}
                $objPHPExcel->getActiveSheet()->getCell('B'.$s)->setValue($list['catname']);
                $objPHPExcel->getActiveSheet()->getCell('C'.$s)->setValue($cat); 
                
             $s++;
             $i++;
			
        }
		//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B'.$s.':F'.$s);
		
      

             //////////////////////////////////////////////
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
// Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="OrderReport-'.date('Ymd h_i_s ').'.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
	}
	
	function xlsPrintOrder_salestock($value,$title){
		$CI     = get_instance();
		$CI->load->model('superadmin/Salestock_Model', 'stock');
		$catprice1 = $CI->stock->catprice1();
		$count=count($value);
		$CI     = get_instance();
        
        $ORDNO       = '#'.str_pad(1, 6, '0', STR_PAD_LEFT);

        // Create new PHPExcel object
        $objPHPExcel            = new PHPExcel();
       // $orderProductsLists     = $CI->Settingmodel->orderProductsList($orderID);
        //$orderInfo              = $CI->Settingmodel->orderInfo($orderID);
        //$countRows              = count($orderProductsLists);

        // $Gtotal                 = $orderInfo->total;
        // $shippingPrice  = '';
        // $shippingPrice1 = 0;
        
        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                     ->setLastModifiedBy("Maarten Balliauw")
                                     ->setTitle("Office 2007 XLSX Test Document")
                                     ->setSubject("Office 2007 XLSX Test Document")
                                     ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                     ->setKeywords("office 2007 openxml php")
                                     ->setCategory("Test result file");


                $main_title_style = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => '31849b'),
                        'size'  => 20,
                        
                    ));
                $smalltitle_style = array(
                    'font'  => array(
                        'bold'  => true,
                        'size'  => 12
                    ),
                    'alignment' => array(
                            'wrap'       => true
                    )
                );
 $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); 
         
        //////////////////////////////////
        
        $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B2:C2');
        $objPHPExcel->getActiveSheet()->getCell('B2')->setValue($title);  
        
        $objPHPExcel->getActiveSheet()->getCell('B3')->setValue('Total no of Entries');
        $objPHPExcel->getActiveSheet()->getCell('C3')->setValue($count);   
        $objPHPExcel->getActiveSheet()->getCell('B5')->setValue('Category');
        $objPHPExcel->getActiveSheet()->getCell('C5')->setValue('Stock');
        
        
        $objPHPExcel->getActiveSheet()->getStyle('B5:C5')->applyFromArray($smalltitle_style);
       
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($main_title_style);
                
        $s          = 6;        
        $i          = 1;
		$totalcost=0;
		$cat = 0;
		foreach ($value as $list) {
				
						foreach($catprice1 as $list1) {				
							if($list1['itemtype']==$list['category_id']){ 
								 if($list1['quantity']!='') { $cat = $list1['quantity']-$list1['qtybal']; } 
							} 
							
						} 
						
						if($cat == ''){
							$cat = 0;
						}
                $objPHPExcel->getActiveSheet()->getCell('B'.$s)->setValue($list['category_name']);
                $objPHPExcel->getActiveSheet()->getCell('C'.$s)->setValue($cat); 
                
             $s++;
             $i++;
			
        }
		//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B'.$s.':F'.$s);
		
      

             //////////////////////////////////////////////
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
// Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="OrderReport-'.date('Ymd h_i_s ').'.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
	}
	
	function xlsPrintOrder_terminal($value,$title){
		$count=count($value);
		$CI     = get_instance();
        
        $ORDNO       = '#'.str_pad(1, 6, '0', STR_PAD_LEFT);

        // Create new PHPExcel object
        $objPHPExcel            = new PHPExcel();
       // $orderProductsLists     = $CI->Settingmodel->orderProductsList($orderID);
        //$orderInfo              = $CI->Settingmodel->orderInfo($orderID);
        //$countRows              = count($orderProductsLists);

        // $Gtotal                 = $orderInfo->total;
        // $shippingPrice  = '';
        // $shippingPrice1 = 0;
        
        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                     ->setLastModifiedBy("Maarten Balliauw")
                                     ->setTitle("Office 2007 XLSX Test Document")
                                     ->setSubject("Office 2007 XLSX Test Document")
                                     ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                     ->setKeywords("office 2007 openxml php")
                                     ->setCategory("Test result file");


                $main_title_style = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => '31849b'),
                        'size'  => 20,
                        
                    ));
                $smalltitle_style = array(
                    'font'  => array(
                        'bold'  => true,
                        'size'  => 12
                    ),
                    'alignment' => array(
                            'wrap'       => true
                    )
                );
 $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);  
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);  
		
        //////////////////////////////////
        
        $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B2:G2');
        $objPHPExcel->getActiveSheet()->getCell('B2')->setValue($title);  
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('E3:F3');
        $objPHPExcel->getActiveSheet()->getCell('E3')->setValue('Total no of Entries');
        $objPHPExcel->getActiveSheet()->getCell('G3')->setValue($count);   
        $objPHPExcel->getActiveSheet()->getCell('B5')->setValue('ID');
        $objPHPExcel->getActiveSheet()->getCell('C5')->setValue('Title');
        $objPHPExcel->getActiveSheet()->getCell('D5')->setValue('Type');
        $objPHPExcel->getActiveSheet()->getCell('E5')->setValue('Area');
        $objPHPExcel->getActiveSheet()->getCell('F5')->setValue('Note');
		$objPHPExcel->getActiveSheet()->getCell('G5')->setValue('Status');

        
        $objPHPExcel->getActiveSheet()->getStyle('B5:C5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('D5:E5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('F5:G5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('H5:I5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('J5:K5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($main_title_style);
                
        $s          = 6;        
        $i          = 1;
		$totalcost=0;
foreach ($value as $list) {
	
	if($list['status']==0) { $sts = 'Vacant'; } else if($list['status']==1) { $sts = 'Used'; } else if($list['status']==2){ $sts = 'Unavailable';
					}
                $objPHPExcel->getActiveSheet()->getCell('B'.$s)->setValue($i);
                $objPHPExcel->getActiveSheet()->getCell('C'.$s)->setValue($list['title']); 
                $objPHPExcel->getActiveSheet()->getCell('D'.$s)->setValue($list['type']);
                $objPHPExcel->getActiveSheet()->getCell('E'.$s)->setValue($list['area']);
				$objPHPExcel->getActiveSheet()->getCell('F'.$s)->setValue($list['notes']);
				$objPHPExcel->getActiveSheet()->getCell('G'.$s)->setValue($sts);
				
             $s++;
             $i++;

        }
		
        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$countRow.':E'.$countRow);
        // $objPHPExcel->getActiveSheet()->getCell('D'.$countRow)->setValue('Total');
        // $objPHPExcel->getActiveSheet()->getCell('F'.$countRow)->setValue($Stotal);

        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$countRow1.':E'.$countRow1);
        // $objPHPExcel->getActiveSheet()->getCell('D'.$countRow1)->setValue('Shipping');
        // $objPHPExcel->getActiveSheet()->getCell('F'.$countRow1)->setValue($shippingPrice);

        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$countRow2 .':E'.$countRow2);
        // $objPHPExcel->getActiveSheet()->getCell('D'.$countRow2)->setValue('Grand Total');
        // $objPHPExcel->getActiveSheet()->getCell('F'.$countRow2)->setValue($Gtotal);
            
             //////////////////////////////////////////////
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
// Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="OrderReport-'.date('Ymd h_i_s ').'.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
	}

	function xlsPrintOrder_terminal_details($value,$title){
		$count=count($value);
		$CI     = get_instance();
        
        $ORDNO       = '#'.str_pad(1, 6, '0', STR_PAD_LEFT);

        // Create new PHPExcel object
        $objPHPExcel            = new PHPExcel();
        
        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                     ->setLastModifiedBy("Maarten Balliauw")
                                     ->setTitle("Office 2007 XLSX Test Document")
                                     ->setSubject("Office 2007 XLSX Test Document")
                                     ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                     ->setKeywords("office 2007 openxml php")
                                     ->setCategory("Test result file");


                $main_title_style = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => '31849b'),
                        'size'  => 20,
                        
                    ));
                $tabletitle_style = array(
		            'font'  => array(
		                'bold'  => true,
		                'color' => array('rgb' => '31849b'),
		                'size'  => 14
		            ),
		            'alignment' => array(
		                    'wrap'       => true
		            )
		        );
                $smalltitle_style = array(
                    'font'  => array(
                        'bold'  => true,
                        'size'  => 12
                    ),
                    'alignment' => array(
                            'wrap'       => true
                    )
                );
 		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);  
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);  
		
        //////////////////////////////////
        
        $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B2:G2');
        $objPHPExcel->getActiveSheet()->getCell('B2')->setValue($title);
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($main_title_style);

        $defaultCount = 5;
    	$editDatacount = 0;
    	$cyclistcount = 0;
    	$mainlistcount = 0;
    	$consumptionlistcount = 0;
    	$productionlistcount = 0;
        foreach ($value as $title => $lines) {
        	
        	switch ($title) {
	        	case 'editData':
        			$editDatacount = count($lines) + 3;

        			$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B4:C4');
			        $objPHPExcel->getActiveSheet()->getCell('B4')->setValue('Terminals Details');
			        $objPHPExcel->getActiveSheet()->getStyle('B4')->applyFromArray($tabletitle_style);
        			
        			$objPHPExcel->getActiveSheet()->getStyle('B'.$defaultCount.':C'.$defaultCount)->applyFromArray($smalltitle_style);
			        $objPHPExcel->getActiveSheet()->getStyle('D'.$defaultCount.':E'.$defaultCount)->applyFromArray($smalltitle_style);
			        $objPHPExcel->getActiveSheet()->getStyle('F'.$defaultCount.':G'.$defaultCount)->applyFromArray($smalltitle_style);

	        		$objPHPExcel->getActiveSheet()->getCell('B'.$defaultCount)->setValue('ID');
			        $objPHPExcel->getActiveSheet()->getCell('C'.$defaultCount)->setValue('Title');
			        $objPHPExcel->getActiveSheet()->getCell('D'.$defaultCount)->setValue('Type');
			        $objPHPExcel->getActiveSheet()->getCell('E'.$defaultCount)->setValue('Area');
			        $objPHPExcel->getActiveSheet()->getCell('F'.$defaultCount)->setValue('Note');
					$objPHPExcel->getActiveSheet()->getCell('G'.$defaultCount)->setValue('Status');

					$editline          = $defaultCount + 1;        
			        $i          = 1;
					foreach ($lines as $list) {
				
						if($list['status']==0){ 
							$sts = 'Vacant'; 
						} else if($list['status']==1) { 
							$sts = 'Used'; 
						} else if($list['status']==2){ 
							$sts = 'Unavailable';
						}

			            $objPHPExcel->getActiveSheet()->getCell('B'.$editline)->setValue($i);
			            $objPHPExcel->getActiveSheet()->getCell('C'.$editline)->setValue($list['title']); 
			            $objPHPExcel->getActiveSheet()->getCell('D'.$editline)->setValue($list['type']);
			            $objPHPExcel->getActiveSheet()->getCell('E'.$editline)->setValue($list['area']);
						$objPHPExcel->getActiveSheet()->getCell('F'.$editline)->setValue($list['notes']);
						$objPHPExcel->getActiveSheet()->getCell('G'.$editline)->setValue($sts);
							
			            $editline++;
			            $i++;
			        }
	        		break;
	        	
	        	case 'cyclist':
	        		$cyclistcount = count($lines) + 3;
	        		$cyclenum = $defaultCount + $editDatacount;

	        		$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B'.($cyclenum - 1).':C'.($cyclenum - 1));
			        $objPHPExcel->getActiveSheet()->getCell('B'.($cyclenum - 1))->setValue('Cycles Details');
			        $objPHPExcel->getActiveSheet()->getStyle('B'.($cyclenum - 1))->applyFromArray($tabletitle_style);

	        		$objPHPExcel->getActiveSheet()->getStyle('B'.$cyclenum.':C'.$cyclenum)->applyFromArray($smalltitle_style);
			        $objPHPExcel->getActiveSheet()->getStyle('D'.$cyclenum.':E'.$cyclenum)->applyFromArray($smalltitle_style);
			        $objPHPExcel->getActiveSheet()->getStyle('F'.$cyclenum.':G'.$cyclenum)->applyFromArray($smalltitle_style);

	        		$objPHPExcel->getActiveSheet()->getCell('B'.$cyclenum)->setValue('ID');
			        $objPHPExcel->getActiveSheet()->getCell('C'.$cyclenum)->setValue('Name');
			        $objPHPExcel->getActiveSheet()->getCell('D'.$cyclenum)->setValue('Start Date');
			        $objPHPExcel->getActiveSheet()->getCell('E'.$cyclenum)->setValue('Expecting End Date');
			        $objPHPExcel->getActiveSheet()->getCell('F'.$cyclenum)->setValue('Actual End Date');

			        $cycleline          = $cyclenum + 1;        
			        $i          = 1;
					foreach ($lines as $list) {
			            $objPHPExcel->getActiveSheet()->getCell('B'.$cycleline)->setValue($i);
			            $objPHPExcel->getActiveSheet()->getCell('C'.$cycleline)->setValue($list['name']); 
			            $objPHPExcel->getActiveSheet()->getCell('D'.$cycleline)->setValue(PHPExcel_Shared_Date::PHPToExcel(strtotime($list['start_date'])));
			            $objPHPExcel->getActiveSheet()->getStyle('D'.$cycleline)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);
			            $objPHPExcel->getActiveSheet()->getCell('E'.$cycleline)->setValue(PHPExcel_Shared_Date::PHPToExcel(strtotime($list['expect_date'])));
			            $objPHPExcel->getActiveSheet()->getStyle('E'.$cycleline)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);
						$objPHPExcel->getActiveSheet()->getCell('F'.$cycleline)->setValue(PHPExcel_Shared_Date::PHPToExcel(strtotime($list['actual_date'])));							
						$objPHPExcel->getActiveSheet()->getStyle('F'.$cycleline)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);
			            $cycleline++;
			            $i++;
			        }
                    break;

                case 'mainlist1':
                	$mainlistcount = count($lines) + 3;
                	$mainnum = $defaultCount + $editDatacount + $cyclistcount;

                	$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B'.($mainnum - 1).':C'.($mainnum - 1));
			        $objPHPExcel->getActiveSheet()->getCell('B'.($mainnum - 1))->setValue('Maintenance');
			        $objPHPExcel->getActiveSheet()->getStyle('B'.($mainnum - 1))->applyFromArray($tabletitle_style);

                	$objPHPExcel->getActiveSheet()->getStyle('B'.$mainnum.':C'.$mainnum)->applyFromArray($smalltitle_style);
			        $objPHPExcel->getActiveSheet()->getStyle('D'.$mainnum.':E'.$mainnum)->applyFromArray($smalltitle_style);
			        $objPHPExcel->getActiveSheet()->getStyle('F'.$mainnum.':G'.$mainnum)->applyFromArray($smalltitle_style);

                	$objPHPExcel->getActiveSheet()->getCell('B'.$mainnum)->setValue('ID');
			        $objPHPExcel->getActiveSheet()->getCell('C'.$mainnum)->setValue('Equipment Name');
			        $objPHPExcel->getActiveSheet()->getCell('D'.$mainnum)->setValue('Problem description');
			        $objPHPExcel->getActiveSheet()->getCell('E'.$mainnum)->setValue('Date');
			        $objPHPExcel->getActiveSheet()->getCell('F'.$mainnum)->setValue('Equipment Status');
					$objPHPExcel->getActiveSheet()->getCell('G'.$mainnum)->setValue('Cost');

					$mainline          = $mainnum + 1;        
			        $i          = 1;
					foreach ($lines as $list) {

						if($list['status']=='0'){
							$mainsts = 'In Progress';
						}else{
							$mainsts = 'Completed';
						}

			            $objPHPExcel->getActiveSheet()->getCell('B'.$mainline)->setValue($i);
			            $objPHPExcel->getActiveSheet()->getCell('C'.$mainline)->setValue($list['name']); 
			            $objPHPExcel->getActiveSheet()->getCell('D'.$mainline)->setValue($list['problem']);
			            $objPHPExcel->getActiveSheet()->getCell('E'.$mainline)->setValue(PHPExcel_Shared_Date::PHPToExcel(strtotime($list['date'])));
			            $objPHPExcel->getActiveSheet()->getStyle('E'.$mainline)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);
						$objPHPExcel->getActiveSheet()->getCell('F'.$mainline)->setValue($list['notes']);
						$objPHPExcel->getActiveSheet()->getCell('G'.$mainline)->setValue($mainsts);
							
			            $mainline++;
			            $i++;
			        }
                    break;

                case 'consumptionlist':
                	$consumptionlistcount = count($lines) + 3;
                	$consumnum = $defaultCount + $editDatacount + $cyclistcount + $mainlistcount;

                	$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B'.($consumnum - 1).':C'.($consumnum - 1));
			        $objPHPExcel->getActiveSheet()->getCell('B'.($consumnum - 1))->setValue('Consumption');
			        $objPHPExcel->getActiveSheet()->getStyle('B'.($consumnum - 1))->applyFromArray($tabletitle_style);

                	$objPHPExcel->getActiveSheet()->getStyle('B'.$consumnum.':C'.$consumnum)->applyFromArray($smalltitle_style);
			        $objPHPExcel->getActiveSheet()->getStyle('D'.$consumnum.':E'.$consumnum)->applyFromArray($smalltitle_style);

                	$objPHPExcel->getActiveSheet()->getCell('B'.$consumnum)->setValue('ID');
			        $objPHPExcel->getActiveSheet()->getCell('C'.$consumnum)->setValue('Category');
			        $objPHPExcel->getActiveSheet()->getCell('D'.$consumnum)->setValue('Quantity');

			        $consumline          = $consumnum + 1;        
			        $i          = 1;
					foreach ($lines as $list) {
			            $objPHPExcel->getActiveSheet()->getCell('B'.$consumline)->setValue($i);
			            $objPHPExcel->getActiveSheet()->getCell('C'.$consumline)->setValue($list['category_name']); 
			            $objPHPExcel->getActiveSheet()->getCell('D'.$consumline)->setValue($list['quantity']);
							
			            $consumline++;
			            $i++;
			        }
                    break;

                case 'productionlist':
                	$productionlistcount = count($lines) + 2;
                	$productnum = $defaultCount + $editDatacount + $cyclistcount + $mainlistcount + $consumptionlistcount;

                	$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B'.($productnum - 1).':C'.($productnum - 1));
			        $objPHPExcel->getActiveSheet()->getCell('B'.($productnum - 1))->setValue('Production');
			        $objPHPExcel->getActiveSheet()->getStyle('B'.($productnum - 1))->applyFromArray($tabletitle_style);

                	$objPHPExcel->getActiveSheet()->getStyle('B'.$productnum.':C'.$productnum)->applyFromArray($smalltitle_style);
			        $objPHPExcel->getActiveSheet()->getStyle('D'.$productnum.':E'.$productnum)->applyFromArray($smalltitle_style);

                	$objPHPExcel->getActiveSheet()->getCell('B'.$productnum)->setValue('ID');
			        $objPHPExcel->getActiveSheet()->getCell('C'.$productnum)->setValue('Category');
			        $objPHPExcel->getActiveSheet()->getCell('D'.$productnum)->setValue('Quantity');

			        $productline          = $productnum + 1;        
			        $i          = 1;
					foreach ($lines as $list) {
			            $objPHPExcel->getActiveSheet()->getCell('B'.$productline)->setValue($i);
			            $objPHPExcel->getActiveSheet()->getCell('C'.$productline)->setValue($list['category_name']); 
			            $objPHPExcel->getActiveSheet()->getCell('D'.$productline)->setValue($list['quantity']);
							
			            $productline++;
			            $i++;
			        }
                    break;
                
                default:
                    return false;
                    break;
	        }
        }
            
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
		// Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="TerminalsDetailsReport-'.date('Ymd h_i_s ').'.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
	}

	function xlsPrintOrder_operation_terminal($value,$title){
		$count=count($value);
		$CI     = get_instance();
        
        $ORDNO       = '#'.str_pad(1, 6, '0', STR_PAD_LEFT);

        // Create new PHPExcel object
        $objPHPExcel            = new PHPExcel();
        
        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                     ->setLastModifiedBy("Maarten Balliauw")
                                     ->setTitle("Office 2007 XLSX Test Document")
                                     ->setSubject("Office 2007 XLSX Test Document")
                                     ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                     ->setKeywords("office 2007 openxml php")
                                     ->setCategory("Test result file");


        $main_title_style = array(
            'font'  => array(
                'bold'  => true,
                'color' => array('rgb' => '31849b'),
                'size'  => 20,
                
            ));
        $smalltitle_style = array(
        	'fill' => array(
	            'type' => PHPExcel_Style_Fill::FILL_SOLID,
	            'color' => array('rgb' => 'ebebeb')
	        ),
            'font'  => array(
                'bold'  => true,
                'size'  => 12
            ),
            'alignment' => array(
                    'wrap'       => true
            )
        );
        $tabletitle_style = array(
            'font'  => array(
                'bold'  => true,
                'color' => array('rgb' => '31849b'),
                'size'  => 14
            ),
            'alignment' => array(
                    'wrap'       => true
            )
        );
 		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);  
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);  
		
        //////////////////////////////////
        
        $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B2:G2');
        $objPHPExcel->getActiveSheet()->getCell('B2')->setValue($title);
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($main_title_style);

        $defaultCount = 6;
    	$editDatacount = 0;
    	$cyclistcount = 0;
    	$mainlistcount = 0;
    	$consumptionlistcount = 0;
    	$productionlistcount = 0;
        foreach ($value as $title => $lines) {
        	
        	switch ($title) {
	        	case 'editData':
        			$editDatacount = count($lines) + 3;

        			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B5:C5');
        			$objPHPExcel->getActiveSheet()->setTitle($lines[0]['title']);
			        $objPHPExcel->getActiveSheet()->getCell('B5')->setValue('Terminals Details');
			        $objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray($tabletitle_style);
        			
        			$objPHPExcel->getActiveSheet()->getStyle('B'.$defaultCount.':C'.$defaultCount)->applyFromArray($smalltitle_style);
			        $objPHPExcel->getActiveSheet()->getStyle('D'.$defaultCount.':E'.$defaultCount)->applyFromArray($smalltitle_style);
			        $objPHPExcel->getActiveSheet()->getStyle('F'.$defaultCount.':G'.$defaultCount)->applyFromArray($smalltitle_style);

	        		$objPHPExcel->getActiveSheet()->getCell('B'.$defaultCount)->setValue('ID');
			        $objPHPExcel->getActiveSheet()->getCell('C'.$defaultCount)->setValue('Title');
			        $objPHPExcel->getActiveSheet()->getCell('D'.$defaultCount)->setValue('Type');
			        $objPHPExcel->getActiveSheet()->getCell('E'.$defaultCount)->setValue('Area');
			        $objPHPExcel->getActiveSheet()->getCell('F'.$defaultCount)->setValue('Note');
					$objPHPExcel->getActiveSheet()->getCell('G'.$defaultCount)->setValue('Status');

					$editline          = $defaultCount + 1;        
			        $i          = 1;
					foreach ($lines as $list) {
				
						if($list['status']==0){ 
							$sts = 'Vacant'; 
						} else if($list['status']==1) { 
							$sts = 'Used'; 
						} else if($list['status']==2){ 
							$sts = 'Unavailable';
						}

			            $objPHPExcel->getActiveSheet()->getCell('B'.$editline)->setValue($i);
			            $objPHPExcel->getActiveSheet()->getCell('C'.$editline)->setValue($list['title']); 
			            $objPHPExcel->getActiveSheet()->getCell('D'.$editline)->setValue($list['type']);
			            $objPHPExcel->getActiveSheet()->getCell('E'.$editline)->setValue($list['area']);
						$objPHPExcel->getActiveSheet()->getCell('F'.$editline)->setValue($list['notes']);
						$objPHPExcel->getActiveSheet()->getCell('G'.$editline)->setValue($sts);
							
			            $editline++;
			            $i++;
			        }
	        		break;
	        	
	        	case 'cyclist':
	        		$cyclistcount = count($lines) + 3;
	        		$cyclenum = $defaultCount + $editDatacount;

	        		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B'.($cyclenum - 1).':C'.($cyclenum - 1));
			        $objPHPExcel->getActiveSheet()->getCell('B'.($cyclenum - 1))->setValue('Cycles Details');
			        $objPHPExcel->getActiveSheet()->getStyle('B'.($cyclenum - 1))->applyFromArray($tabletitle_style);

	        		$objPHPExcel->getActiveSheet()->getStyle('B'.$cyclenum.':C'.$cyclenum)->applyFromArray($smalltitle_style);
			        $objPHPExcel->getActiveSheet()->getStyle('D'.$cyclenum.':E'.$cyclenum)->applyFromArray($smalltitle_style);
			        $objPHPExcel->getActiveSheet()->getStyle('F'.$cyclenum.':G'.$cyclenum)->applyFromArray($smalltitle_style);

	        		$objPHPExcel->getActiveSheet()->getCell('B'.$cyclenum)->setValue('ID');
			        $objPHPExcel->getActiveSheet()->getCell('C'.$cyclenum)->setValue('Name');
			        $objPHPExcel->getActiveSheet()->getCell('D'.$cyclenum)->setValue('Start Date');
			        $objPHPExcel->getActiveSheet()->getCell('E'.$cyclenum)->setValue('Expecting End Date');
			        $objPHPExcel->getActiveSheet()->getCell('F'.$cyclenum)->setValue('Actual End Date');

			        $cycleline          = $cyclenum + 1;        
			        $i          = 1;
					foreach ($lines as $list) {
			            $objPHPExcel->getActiveSheet()->getCell('B'.$cycleline)->setValue($i);
			            $objPHPExcel->getActiveSheet()->getCell('C'.$cycleline)->setValue($list['name']); 
			            $objPHPExcel->getActiveSheet()->getCell('D'.$cycleline)->setValue(PHPExcel_Shared_Date::PHPToExcel(strtotime($list['start_date'])) );
			            $objPHPExcel->getActiveSheet()->getStyle('D'.$cycleline)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);
			            $objPHPExcel->getActiveSheet()->getCell('E'.$cycleline)->setValue(PHPExcel_Shared_Date::PHPToExcel(strtotime($list['expect_date'])));
			            $objPHPExcel->getActiveSheet()->getStyle('E'.$cycleline)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);
						$objPHPExcel->getActiveSheet()->getCell('F'.$cycleline)->setValue(PHPExcel_Shared_Date::PHPToExcel(strtotime($list['actual_date'])));							
						$objPHPExcel->getActiveSheet()->getStyle('F'.$cycleline)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);
			            $cycleline++;
			            $i++;
			        }
                    break;

                case 'mainlist':
                	$mainlistcount = count($lines) + 3;
                	$mainnum = $defaultCount + $editDatacount + $cyclistcount;

                	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B'.($mainnum - 1).':C'.($mainnum - 1));
			        $objPHPExcel->getActiveSheet()->getCell('B'.($mainnum - 1))->setValue('Maintenance');
			        $objPHPExcel->getActiveSheet()->getStyle('B'.($mainnum - 1))->applyFromArray($tabletitle_style);

                	$objPHPExcel->getActiveSheet()->getStyle('B'.$mainnum.':C'.$mainnum)->applyFromArray($smalltitle_style);
			        $objPHPExcel->getActiveSheet()->getStyle('D'.$mainnum.':E'.$mainnum)->applyFromArray($smalltitle_style);
			        $objPHPExcel->getActiveSheet()->getStyle('F'.$mainnum.':G'.$mainnum)->applyFromArray($smalltitle_style);

                	$objPHPExcel->getActiveSheet()->getCell('B'.$mainnum)->setValue('ID');
			        $objPHPExcel->getActiveSheet()->getCell('C'.$mainnum)->setValue('Equipment Name');
			        $objPHPExcel->getActiveSheet()->getCell('D'.$mainnum)->setValue('Problem description');
			        $objPHPExcel->getActiveSheet()->getCell('E'.$mainnum)->setValue('Date');
			        $objPHPExcel->getActiveSheet()->getCell('F'.$mainnum)->setValue('Equipment Status');
					$objPHPExcel->getActiveSheet()->getCell('G'.$mainnum)->setValue('Cost');

					$mainline          = $mainnum + 1;        
			        $i          = 1;
					foreach ($lines as $list) {

						if($list['status']=='0'){
							$mainsts = 'In Progress';
						}else{
							$mainsts = 'Completed';
						}

			            $objPHPExcel->getActiveSheet()->getCell('B'.$mainline)->setValue($i);
			            $objPHPExcel->getActiveSheet()->getCell('C'.$mainline)->setValue($list['name']); 
			            $objPHPExcel->getActiveSheet()->getCell('D'.$mainline)->setValue($list['problem']);
			            $objPHPExcel->getActiveSheet()->getCell('E'.$mainline)->setValue(PHPExcel_Shared_Date::PHPToExcel(strtotime($list['date'])));
			            $objPHPExcel->getActiveSheet()->getStyle('E'.$mainline)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);
						$objPHPExcel->getActiveSheet()->getCell('F'.$mainline)->setValue($mainsts);
						$objPHPExcel->getActiveSheet()->getCell('G'.$mainline)->setValue($list['cost']);
							
			            $mainline++;
			            $i++;
			        }
                    break;

                case 'consumptionlist':
                	$consumptionlistcount = count($lines) + 2;
                	$consumnum = $defaultCount + $editDatacount + $cyclistcount + $mainlistcount;

                	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B'.($consumnum - 1).':C'.($consumnum - 1));
			        $objPHPExcel->getActiveSheet()->getCell('B'.($consumnum - 1))->setValue('Consumption');
			        $objPHPExcel->getActiveSheet()->getStyle('B'.($consumnum - 1))->applyFromArray($tabletitle_style);

                	$objPHPExcel->getActiveSheet()->getStyle('B'.$consumnum.':I'.$consumnum)->applyFromArray($smalltitle_style);

                	$objPHPExcel->getActiveSheet()->getCell('B'.$consumnum)->setValue('ID');
			        $objPHPExcel->getActiveSheet()->getCell('C'.$consumnum)->setValue('Terminal Name');
			        $objPHPExcel->getActiveSheet()->getCell('D'.$consumnum)->setValue('Cycle Name');
			        $objPHPExcel->getActiveSheet()->getCell('E'.$consumnum)->setValue('Category');
			        $objPHPExcel->getActiveSheet()->getCell('F'.$consumnum)->setValue('Date');
			        $objPHPExcel->getActiveSheet()->getCell('G'.$consumnum)->setValue('Quantity');
			        $objPHPExcel->getActiveSheet()->getCell('H'.$consumnum)->setValue('Unit Price');
			        $objPHPExcel->getActiveSheet()->getCell('I'.$consumnum)->setValue('Total Price');

			        $totalPrice = 0;
					$quantity = 0;
					$unitprice = 0;
					$paid = 0;
			        $consumline          = $consumnum + 1;        
			        $i          = 1;
			        $cyclelist_main = array();
			        $concatlist_main = array();

					if(isset($lines['cyclelist'])){
						$cyclelist_main = $lines['cyclelist'];
						unset($lines['cyclelist']);
					}
					if(isset($lines['concatlist'])){
						$concatlist_main = $lines['concatlist'];
						unset($lines['concatlist']);
					}
					
					foreach ($lines as $key => $list) {
						$totalPrice = $list['quantity']*$list['avgprice'];
			            $objPHPExcel->getActiveSheet()->getCell('B'.$consumline)->setValue($i);
			            $objPHPExcel->getActiveSheet()->getCell('C'.$consumline)->setValue($list['title']); 
			            foreach($cyclelist_main as $cyclelist){
			            	if($cyclelist['id']==$list['cyclename']) {
			            		$objPHPExcel->getActiveSheet()->getCell('D'.$consumline)->setValue($cyclelist['name']); 
			            	}
			            }
			            foreach($concatlist_main as $concatlist){
			            	if($concatlist['id']==$list['category']) {
			            		$objPHPExcel->getActiveSheet()->getCell('E'.$consumline)->setValue($concatlist['catname']); 
			            	}
			            }
			            $objPHPExcel->getActiveSheet()->getCell('F'.$consumline)->setValue(PHPExcel_Shared_Date::PHPToExcel(strtotime($list['date']))); 
			            $objPHPExcel->getActiveSheet()->getStyle('F'.$consumline)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);
			            $objPHPExcel->getActiveSheet()->getCell('G'.$consumline)->setValue($list['quantity']);
			            $objPHPExcel->getActiveSheet()->getCell('H'.$consumline)->setValue(round($list['avgprice'],2));
			            $objPHPExcel->getActiveSheet()->getCell('I'.$consumline)->setValue(round($totalPrice,2));
						
						$quantity=$quantity+$list["quantity"];
						$unitprice=$unitprice+$list["avgprice"];
						$paid=$paid+$totalPrice;
			            $consumline++;
			            $i++;
			        }
			        $objPHPExcel->getActiveSheet()->getCell('F'.$consumline)->setValue('Total');
					$objPHPExcel->getActiveSheet()->getCell('G'.$consumline)->setValue($quantity);
					$objPHPExcel->getActiveSheet()->getCell('H'.$consumline)->setValue(round($unitprice,2));
					$objPHPExcel->getActiveSheet()->getCell('I'.$consumline)->setValue(round($paid,2));
					$objPHPExcel->getActiveSheet()->getStyle('F'.$consumline.':I'.$consumline)->applyFromArray($smalltitle_style);
                    break;

                case 'productionlist':
                	$productionlistcount = count($lines);
                	$productnum = $defaultCount + $editDatacount + $cyclistcount + $mainlistcount + $consumptionlistcount;

                	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B'.($productnum - 1).':C'.($productnum - 1));
			        $objPHPExcel->getActiveSheet()->getCell('B'.($productnum - 1))->setValue('Production');
			        $objPHPExcel->getActiveSheet()->getStyle('B'.($productnum - 1))->applyFromArray($tabletitle_style);

                	$objPHPExcel->getActiveSheet()->getStyle('B'.$productnum.':I'.$productnum)->applyFromArray($smalltitle_style);

                	$objPHPExcel->getActiveSheet()->getCell('B'.$productnum)->setValue('ID');
			        $objPHPExcel->getActiveSheet()->getCell('C'.$productnum)->setValue('Terminal Name');
			        $objPHPExcel->getActiveSheet()->getCell('D'.$productnum)->setValue('Cycle Name');
			        $objPHPExcel->getActiveSheet()->getCell('E'.$productnum)->setValue('Item Type');
			        $objPHPExcel->getActiveSheet()->getCell('F'.$productnum)->setValue('Date');
			        $objPHPExcel->getActiveSheet()->getCell('G'.$productnum)->setValue('Quantity');
			        $objPHPExcel->getActiveSheet()->getCell('H'.$productnum)->setValue('Unit Price');
			        $objPHPExcel->getActiveSheet()->getCell('I'.$productnum)->setValue('Total Price');

			        $totalPrice = 0;
					$quantity = 0;
					$unitprice = 0;
					$paid = 0;
			        $productline          = $productnum + 1;        
			        $i          = 1;
			        $terminallist_main = array();
			        $cyclelist_main = array();
			        $procatlist_main = array();

					if(isset($lines['terminallist'])){
						$terminallist_main = $lines['terminallist'];
						unset($lines['terminallist']);
					}
					if(isset($lines['cyclelist'])){
						$cyclelist_main = $lines['cyclelist'];
						unset($lines['cyclelist']);
					}
					if(isset($lines['procatlist'])){
						$procatlist_main = $lines['procatlist'];
						unset($lines['procatlist']);
					}

					foreach ($lines as $key => $list) {
						$totalPrice = $list['quantity']*$list['avgprice'];
			            $objPHPExcel->getActiveSheet()->getCell('B'.$productline)->setValue($i);
			            foreach($terminallist_main as $terminallist){
			            	if($terminallist['id'] == $list['terminalname']) {
			            		$objPHPExcel->getActiveSheet()->getCell('C'.$productline)->setValue($terminallist['title']); 
			            	}
			            }
			            foreach($cyclelist_main as $cyclelist){
			            	if($cyclelist['id'] == $list['cyclename']) {
			            		$objPHPExcel->getActiveSheet()->getCell('D'.$productline)->setValue($cyclelist['name']); 
			            	}
			            }
			            foreach($procatlist_main as $procatlist){
			            	if($procatlist['category_id'] == $list['itemtype']) {
			            		$objPHPExcel->getActiveSheet()->getCell('E'.$productline)->setValue($procatlist['category_name']); 
			            	}
			            }
			            $objPHPExcel->getActiveSheet()->getCell('F'.$productline)->setValue(PHPExcel_Shared_Date::PHPToExcel(strtotime($list['date']))); 
			            $objPHPExcel->getActiveSheet()->getStyle('F'.$productline)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);
			            $objPHPExcel->getActiveSheet()->getCell('G'.$productline)->setValue($list['quantity']); 
			            $objPHPExcel->getActiveSheet()->getCell('H'.$productline)->setValue(round($list['avgprice'],2)); 
			            $objPHPExcel->getActiveSheet()->getCell('I'.$productline)->setValue(round($totalPrice,2));
						
						$quantity=$quantity+$list["quantity"];
						$unitprice=$unitprice+$list["avgprice"];
						$paid=$paid+$totalPrice;	
			            $productline++;
			            $i++;
			        }
			        $objPHPExcel->getActiveSheet()->getCell('F'.$productline)->setValue('Total');
					$objPHPExcel->getActiveSheet()->getCell('G'.$productline)->setValue($quantity);
					$objPHPExcel->getActiveSheet()->getCell('H'.$productline)->setValue(round($unitprice,2));
					$objPHPExcel->getActiveSheet()->getCell('I'.$productline)->setValue(round($paid,2));
					$objPHPExcel->getActiveSheet()->getStyle('F'.$productline.':I'.$productline)->applyFromArray($smalltitle_style);
                    break;
                
                default:
                    return false;
                    break;
	        }
        }

        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="TerminalsDetailsReport-'.date('Ymd h_i_s ').'.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
	}

    function xlsPrintOrder_operation_terminal_multiple($terminals,$headline){
		$count=count($terminals);
		$CI     = get_instance();
        
        $ORDNO       = '#'.str_pad(1, 6, '0', STR_PAD_LEFT);

        // Create new PHPExcel object
        $objPHPExcel            = new PHPExcel();

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                     ->setLastModifiedBy("Maarten Balliauw")
                                     ->setTitle("Office 2007 XLSX Test Document")
                                     ->setSubject("Office 2007 XLSX Test Document")
                                     ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                     ->setKeywords("office 2007 openxml php")
                                     ->setCategory("Test result file");

        $main_title_style = array(
            'font'  => array(
                'bold'  => true,
                'color' => array('rgb' => '31849b'),
                'size'  => 20,
                
            ));
        $smalltitle_style = array(
        	'fill' => array(
	            'type' => PHPExcel_Style_Fill::FILL_SOLID,
	            'color' => array('rgb' => 'ebebeb')
	        ),
            'font'  => array(
                'bold'  => true,
                'size'  => 12
            ),
            'alignment' => array(
                    'wrap'       => true
            )
        );
        $tabletitle_style = array(
            'font'  => array(
                'bold'  => true,
                'color' => array('rgb' => '31849b'),
                'size'  => 14
            ),
            'alignment' => array(
                    'wrap'       => true
            )
        );

    	foreach ($terminals as $sheet => $value) {
    		$objPHPExcel->setActiveSheetIndex($sheet);

	 		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); 
	        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); 
	        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); 
	        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); 
	        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);  
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);  
			
	        //////////////////////////////////
	        
	        $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
	        $objPHPExcel->setActiveSheetIndex($sheet)->mergeCells('B2:G2');
	        $objPHPExcel->getActiveSheet()->getCell('B2')->setValue($headline);
	        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($main_title_style);

	        $defaultCount = 6;
	    	$editDatacount = 0;
	    	$cyclistcount = 0;
	    	$mainlistcount = 0;
	    	$consumptionlistcount = 0;
	    	$productionlistcount = 0;
	        foreach ($value as $title => $lines) {
	        	
	        	switch ($title) {
		        	case 'editData':
	        			$editDatacount = count($lines) + 3;

	        			$objPHPExcel->setActiveSheetIndex($sheet)->mergeCells('B5:C5');
	        			$objPHPExcel->getActiveSheet()->setTitle($lines[0]['title']);
				        $objPHPExcel->getActiveSheet()->getCell('B5')->setValue('Terminals Details');
				        $objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray($tabletitle_style);
	        			
	        			$objPHPExcel->getActiveSheet()->getStyle('B'.$defaultCount.':C'.$defaultCount)->applyFromArray($smalltitle_style);
				        $objPHPExcel->getActiveSheet()->getStyle('D'.$defaultCount.':E'.$defaultCount)->applyFromArray($smalltitle_style);
				        $objPHPExcel->getActiveSheet()->getStyle('F'.$defaultCount.':G'.$defaultCount)->applyFromArray($smalltitle_style);

		        		$objPHPExcel->getActiveSheet()->getCell('B'.$defaultCount)->setValue('ID');
				        $objPHPExcel->getActiveSheet()->getCell('C'.$defaultCount)->setValue('Title');
				        $objPHPExcel->getActiveSheet()->getCell('D'.$defaultCount)->setValue('Type');
				        $objPHPExcel->getActiveSheet()->getCell('E'.$defaultCount)->setValue('Area');
				        $objPHPExcel->getActiveSheet()->getCell('F'.$defaultCount)->setValue('Note');
						$objPHPExcel->getActiveSheet()->getCell('G'.$defaultCount)->setValue('Status');

						$editline          = $defaultCount + 1;        
				        $i          = 1;
						foreach ($lines as $list) {
					
							if($list['status']==0){ 
								$sts = 'Vacant'; 
							} else if($list['status']==1) { 
								$sts = 'Used'; 
							} else if($list['status']==2){ 
								$sts = 'Unavailable';
							}

				            $objPHPExcel->getActiveSheet()->getCell('B'.$editline)->setValue($i);
				            $objPHPExcel->getActiveSheet()->getCell('C'.$editline)->setValue($list['title']); 
				            $objPHPExcel->getActiveSheet()->getCell('D'.$editline)->setValue($list['type']);
				            $objPHPExcel->getActiveSheet()->getCell('E'.$editline)->setValue($list['area']);
							$objPHPExcel->getActiveSheet()->getCell('F'.$editline)->setValue($list['notes']);
							$objPHPExcel->getActiveSheet()->getCell('G'.$editline)->setValue($sts);
								
				            $editline++;
				            $i++;
				        }
		        		break;
		        	
		        	case 'cyclist':
		        		$cyclistcount = count($lines) + 3;
		        		$cyclenum = $defaultCount + $editDatacount;

		        		$objPHPExcel->setActiveSheetIndex($sheet)->mergeCells('B'.($cyclenum - 1).':C'.($cyclenum - 1));
				        $objPHPExcel->getActiveSheet()->getCell('B'.($cyclenum - 1))->setValue('Cycles Details');
				        $objPHPExcel->getActiveSheet()->getStyle('B'.($cyclenum - 1))->applyFromArray($tabletitle_style);

		        		$objPHPExcel->getActiveSheet()->getStyle('B'.$cyclenum.':C'.$cyclenum)->applyFromArray($smalltitle_style);
				        $objPHPExcel->getActiveSheet()->getStyle('D'.$cyclenum.':E'.$cyclenum)->applyFromArray($smalltitle_style);
				        $objPHPExcel->getActiveSheet()->getStyle('F'.$cyclenum.':G'.$cyclenum)->applyFromArray($smalltitle_style);
				        // $objPHPExcel->getActiveSheet()->getStyle('D11')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);

		        		$objPHPExcel->getActiveSheet()->getCell('B'.$cyclenum)->setValue('ID');
				        $objPHPExcel->getActiveSheet()->getCell('C'.$cyclenum)->setValue('Name');
				        $objPHPExcel->getActiveSheet()->getCell('D'.$cyclenum)->setValue('Start Date');
				        $objPHPExcel->getActiveSheet()->getCell('E'.$cyclenum)->setValue('Expecting End Date');
				        $objPHPExcel->getActiveSheet()->getCell('F'.$cyclenum)->setValue('Actual End Date');

				        $cycleline          = $cyclenum + 1;        
				        $i          = 1;
						foreach ($lines as $list) {
				            $objPHPExcel->getActiveSheet()->getCell('B'.$cycleline)->setValue($i);
				            $objPHPExcel->getActiveSheet()->getCell('C'.$cycleline)->setValue($list['name']);
				            // $objPHPExcel->getActiveSheet()->getCell('D'.$cycleline)->setValue(date("d-m-Y", strtotime($list['start_date'])));
				            $objPHPExcel->getActiveSheet()->getCell('D'.$cycleline)->setValue(PHPExcel_Shared_Date::PHPToExcel(strtotime($list['start_date'])) );
				            $objPHPExcel->getActiveSheet()->getStyle('D'.$cycleline)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);
				            $objPHPExcel->getActiveSheet()->getCell('E'.$cycleline)->setValue(PHPExcel_Shared_Date::PHPToExcel(strtotime($list['expect_date'])));
				            $objPHPExcel->getActiveSheet()->getStyle('E'.$cycleline)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);
							$objPHPExcel->getActiveSheet()->getCell('F'.$cycleline)->setValue(PHPExcel_Shared_Date::PHPToExcel(strtotime($list['actual_date'])));							
							$objPHPExcel->getActiveSheet()->getStyle('F'.$cycleline)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);		
				            $cycleline++;
				            $i++;
				        }
	                    break;

	                case 'mainlist':
	                	$mainlistcount = count($lines) + 3;
	                	$mainnum = $defaultCount + $editDatacount + $cyclistcount;

	                	$objPHPExcel->setActiveSheetIndex($sheet)->mergeCells('B'.($mainnum - 1).':C'.($mainnum - 1));
				        $objPHPExcel->getActiveSheet()->getCell('B'.($mainnum - 1))->setValue('Maintenance');
				        $objPHPExcel->getActiveSheet()->getStyle('B'.($mainnum - 1))->applyFromArray($tabletitle_style);

	                	$objPHPExcel->getActiveSheet()->getStyle('B'.$mainnum.':C'.$mainnum)->applyFromArray($smalltitle_style);
				        $objPHPExcel->getActiveSheet()->getStyle('D'.$mainnum.':E'.$mainnum)->applyFromArray($smalltitle_style);
				        $objPHPExcel->getActiveSheet()->getStyle('F'.$mainnum.':G'.$mainnum)->applyFromArray($smalltitle_style);

	                	$objPHPExcel->getActiveSheet()->getCell('B'.$mainnum)->setValue('ID');
				        $objPHPExcel->getActiveSheet()->getCell('C'.$mainnum)->setValue('Equipment Name');
				        $objPHPExcel->getActiveSheet()->getCell('D'.$mainnum)->setValue('Problem description');
				        $objPHPExcel->getActiveSheet()->getCell('E'.$mainnum)->setValue('Date');
				        $objPHPExcel->getActiveSheet()->getCell('F'.$mainnum)->setValue('Equipment Status');
						$objPHPExcel->getActiveSheet()->getCell('G'.$mainnum)->setValue('Cost');

						$mainline          = $mainnum + 1;        
				        $i          = 1;
						foreach ($lines as $list) {

							if($list['status']=='0'){
								$mainsts = 'In Progress';
							}else{
								$mainsts = 'Completed';
							}

				            $objPHPExcel->getActiveSheet()->getCell('B'.$mainline)->setValue($i);
				            $objPHPExcel->getActiveSheet()->getCell('C'.$mainline)->setValue($list['name']); 
				            $objPHPExcel->getActiveSheet()->getCell('D'.$mainline)->setValue($list['problem']);
				            $objPHPExcel->getActiveSheet()->getCell('E'.$mainline)->setValue(PHPExcel_Shared_Date::PHPToExcel(strtotime($list['date'])));
				            $objPHPExcel->getActiveSheet()->getStyle('E'.$mainline)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);
							$objPHPExcel->getActiveSheet()->getCell('F'.$mainline)->setValue($mainsts);
							$objPHPExcel->getActiveSheet()->getCell('G'.$mainline)->setValue($list['cost']);
								
				            $mainline++;
				            $i++;
				        }
	                    break;

	                case 'consumptionlist':
	                	$consumptionlistcount = count($lines) + 2;
	                	$consumnum = $defaultCount + $editDatacount + $cyclistcount + $mainlistcount;

	                	$objPHPExcel->setActiveSheetIndex($sheet)->mergeCells('B'.($consumnum - 1).':C'.($consumnum - 1));
				        $objPHPExcel->getActiveSheet()->getCell('B'.($consumnum - 1))->setValue('Consumption');
				        $objPHPExcel->getActiveSheet()->getStyle('B'.($consumnum - 1))->applyFromArray($tabletitle_style);

	                	$objPHPExcel->getActiveSheet()->getStyle('B'.$consumnum.':I'.$consumnum)->applyFromArray($smalltitle_style);

	                	$objPHPExcel->getActiveSheet()->getCell('B'.$consumnum)->setValue('ID');
				        $objPHPExcel->getActiveSheet()->getCell('C'.$consumnum)->setValue('Terminal Name');
				        $objPHPExcel->getActiveSheet()->getCell('D'.$consumnum)->setValue('Cycle Name');
				        $objPHPExcel->getActiveSheet()->getCell('E'.$consumnum)->setValue('Category');
				        $objPHPExcel->getActiveSheet()->getCell('F'.$consumnum)->setValue('Date');
				        $objPHPExcel->getActiveSheet()->getCell('G'.$consumnum)->setValue('Quantity');
				        $objPHPExcel->getActiveSheet()->getCell('H'.$consumnum)->setValue('Unit Price');
				        $objPHPExcel->getActiveSheet()->getCell('I'.$consumnum)->setValue('Total Price');

				        $totalPrice = 0;
						$quantity = 0;
						$unitprice = 0;
						$paid = 0;
				        $consumline          = $consumnum + 1;        
				        $i          = 1;
				        $cyclelist_main = array();
				        $concatlist_main = array();

						if(isset($lines['cyclelist'])){
							$cyclelist_main = $lines['cyclelist'];
							unset($lines['cyclelist']);
						}
						if(isset($lines['concatlist'])){
							$concatlist_main = $lines['concatlist'];
							unset($lines['concatlist']);
						}
						
						foreach ($lines as $key => $list) {
							$totalPrice = $list['quantity']*$list['avgprice'];
				            $objPHPExcel->getActiveSheet()->getCell('B'.$consumline)->setValue($i);
				            $objPHPExcel->getActiveSheet()->getCell('C'.$consumline)->setValue($list['title']); 
				            foreach($cyclelist_main as $cyclelist){
				            	if($cyclelist['id']==$list['cyclename']) {
				            		$objPHPExcel->getActiveSheet()->getCell('D'.$consumline)->setValue($cyclelist['name']); 
				            	}
				            }
				            foreach($concatlist_main as $concatlist){
				            	if($concatlist['id']==$list['category']) {
				            		$objPHPExcel->getActiveSheet()->getCell('E'.$consumline)->setValue($concatlist['catname']); 
				            	}
				            }
				            $objPHPExcel->getActiveSheet()->getCell('F'.$consumline)->setValue(PHPExcel_Shared_Date::PHPToExcel(strtotime($list['date']))); 
				            $objPHPExcel->getActiveSheet()->getStyle('F'.$consumline)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);
				            $objPHPExcel->getActiveSheet()->getCell('G'.$consumline)->setValue($list['quantity']);
				            $objPHPExcel->getActiveSheet()->getCell('H'.$consumline)->setValue(round($list['avgprice'],2));
				            $objPHPExcel->getActiveSheet()->getCell('I'.$consumline)->setValue(round($totalPrice,2));
							
							$quantity=$quantity+$list["quantity"];
							$unitprice=$unitprice+$list["avgprice"];
							$paid=$paid+$totalPrice;
				            $consumline++;
				            $i++;
				        }
				        $objPHPExcel->getActiveSheet()->getCell('F'.$consumline)->setValue('Total');
						$objPHPExcel->getActiveSheet()->getCell('G'.$consumline)->setValue($quantity);
						$objPHPExcel->getActiveSheet()->getCell('H'.$consumline)->setValue(round($unitprice,2));
						$objPHPExcel->getActiveSheet()->getCell('I'.$consumline)->setValue(round($paid,2));
						$objPHPExcel->getActiveSheet()->getStyle('F'.$consumline.':I'.$consumline)->applyFromArray($smalltitle_style);
	                    break;

	                case 'productionlist':
	                	$productionlistcount = count($lines);
	                	$productnum = $defaultCount + $editDatacount + $cyclistcount + $mainlistcount + $consumptionlistcount;

	                	$objPHPExcel->setActiveSheetIndex($sheet)->mergeCells('B'.($productnum - 1).':C'.($productnum - 1));
				        $objPHPExcel->getActiveSheet()->getCell('B'.($productnum - 1))->setValue('Production');
				        $objPHPExcel->getActiveSheet()->getStyle('B'.($productnum - 1))->applyFromArray($tabletitle_style);

	                	$objPHPExcel->getActiveSheet()->getStyle('B'.$productnum.':I'.$productnum)->applyFromArray($smalltitle_style);

	                	$objPHPExcel->getActiveSheet()->getCell('B'.$productnum)->setValue('ID');
				        $objPHPExcel->getActiveSheet()->getCell('C'.$productnum)->setValue('Terminal Name');
				        $objPHPExcel->getActiveSheet()->getCell('D'.$productnum)->setValue('Cycle Name');
				        $objPHPExcel->getActiveSheet()->getCell('E'.$productnum)->setValue('Item Type');
				        $objPHPExcel->getActiveSheet()->getCell('F'.$productnum)->setValue('Date');
				        $objPHPExcel->getActiveSheet()->getCell('G'.$productnum)->setValue('Quantity');
				        $objPHPExcel->getActiveSheet()->getCell('H'.$productnum)->setValue('Unit Price');
				        $objPHPExcel->getActiveSheet()->getCell('I'.$productnum)->setValue('Total Price');

				        $totalPrice = 0;
						$quantity = 0;
						$unitprice = 0;
						$paid = 0;
				        $productline          = $productnum + 1;        
				        $i          = 1;
				        $terminallist_main = array();
				        $cyclelist_main = array();
				        $procatlist_main = array();

						if(isset($lines['terminallist'])){
							$terminallist_main = $lines['terminallist'];
							unset($lines['terminallist']);
						}
						if(isset($lines['cyclelist'])){
							$cyclelist_main = $lines['cyclelist'];
							unset($lines['cyclelist']);
						}
						if(isset($lines['procatlist'])){
							$procatlist_main = $lines['procatlist'];
							unset($lines['procatlist']);
						}

						foreach ($lines as $key => $list) {
							$totalPrice = $list['quantity']*$list['avgprice'];
				            $objPHPExcel->getActiveSheet()->getCell('B'.$productline)->setValue($i);
				            foreach($terminallist_main as $terminallist){
				            	if($terminallist['id'] == $list['terminalname']) {
				            		$objPHPExcel->getActiveSheet()->getCell('C'.$productline)->setValue($terminallist['title']); 
				            	}
				            }
				            foreach($cyclelist_main as $cyclelist){
				            	if($cyclelist['id'] == $list['cyclename']) {
				            		$objPHPExcel->getActiveSheet()->getCell('D'.$productline)->setValue($cyclelist['name']); 
				            	}
				            }
				            foreach($procatlist_main as $procatlist){
				            	if($procatlist['category_id'] == $list['itemtype']) {
				            		$objPHPExcel->getActiveSheet()->getCell('E'.$productline)->setValue($procatlist['category_name']); 
				            	}
				            }
				            $objPHPExcel->getActiveSheet()->getCell('F'.$productline)->setValue(PHPExcel_Shared_Date::PHPToExcel(strtotime($list['date']))); 
				            $objPHPExcel->getActiveSheet()->getStyle('F'.$productline)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);
				            $objPHPExcel->getActiveSheet()->getCell('G'.$productline)->setValue($list['quantity']); 
				            $objPHPExcel->getActiveSheet()->getCell('H'.$productline)->setValue(round($list['avgprice'],2)); 
				            $objPHPExcel->getActiveSheet()->getCell('I'.$productline)->setValue(round($totalPrice,2));
							
							$quantity=$quantity+$list["quantity"];
							$unitprice=$unitprice+$list["avgprice"];
							$paid=$paid+$totalPrice;	
				            $productline++;
				            $i++;
				        }
				        $objPHPExcel->getActiveSheet()->getCell('F'.$productline)->setValue('Total');
						$objPHPExcel->getActiveSheet()->getCell('G'.$productline)->setValue($quantity);
						$objPHPExcel->getActiveSheet()->getCell('H'.$productline)->setValue(round($unitprice,2));
						$objPHPExcel->getActiveSheet()->getCell('I'.$productline)->setValue(round($paid,2));
						$objPHPExcel->getActiveSheet()->getStyle('F'.$productline.':I'.$productline)->applyFromArray($smalltitle_style);
	                    break;
	                
	                default:
	                    return false;
	                    break;
		        }
	        }
        	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
        	// print_r($sheet);
        	if(($count - 1) > $sheet){
        		$objPHPExcel->createSheet();
        	}
	    }
            
		// Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="TerminalsDetailsReport-'.date('Ymd h_i_s ').'.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
	}
	
	function xlsPrintOrder_sercus($type,$data,$srch){
		
		$CI     = get_instance();
        
        $ORDNO       = '#'.str_pad(1, 6, '0', STR_PAD_LEFT);

		$CI->load->model('salesadmin/Searchcustomer_Model', 'sales');

        
       // $ORDNO       = '#'.str_pad(1, 6, '0', STR_PAD_LEFT);
		
        // Create new PHPExcel object
        $objPHPExcel            = new PHPExcel();
       // $orderProductsLists     = $CI->Settingmodel->orderProductsList($orderID);
        //$orderInfo              = $CI->Settingmodel->orderInfo($orderID);
        //$countRows              = count($orderProductsLists);

        // $Gtotal                 = $orderInfo->total;
        // $shippingPrice  = '';
        // $shippingPrice1 = 0;
        
		
        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                     ->setLastModifiedBy("Maarten Balliauw")
                                     ->setTitle("Office 2007 XLSX Test Document")
                                     ->setSubject("Office 2007 XLSX Test Document")
                                     ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                     ->setKeywords("office 2007 openxml php")
                                     ->setCategory("Test result file");


                $main_title_style = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => '31849b'),
                        'size'  => 20,
                        
                    ));
                $smalltitle_style = array(
                    'font'  => array(
                        'bold'  => true,
                        'size'  => 12
                    ),
                    'alignment' => array(
                            'wrap'       => true
                    )
                );
				
				
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true); 
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true); 
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true); 
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$srchitemlist = $CI->sales->srchitemlist($type,$srch);
		//echo '<pre>'; print_r($srchitemlist); echo '</pre>'; die;	
		$categorylist = $CI->sales->categorylist();
		$cuslist = $CI->sales->cuslist();
		
		$countRows = count($srchitemlist);
		
        $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B2:J2');
        $objPHPExcel->getActiveSheet()->getCell('B2')->setValue('Sales Item Details');  
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D3:E3');
        $objPHPExcel->getActiveSheet()->getCell('D3')->setValue('Total no of Entries');
        $objPHPExcel->getActiveSheet()->getCell('F3')->setValue($countRows);   
        $objPHPExcel->getActiveSheet()->getCell('B5')->setValue('SI Number');
        $objPHPExcel->getActiveSheet()->getCell('C5')->setValue('Bill');
        $objPHPExcel->getActiveSheet()->getCell('D5')->setValue('Category');
        $objPHPExcel->getActiveSheet()->getCell('E5')->setValue('Customer Name');
        $objPHPExcel->getActiveSheet()->getCell('F5')->setValue('Date');
		$objPHPExcel->getActiveSheet()->getCell('G5')->setValue('Price');
		$objPHPExcel->getActiveSheet()->getCell('H5')->setValue('Quantity');
		$objPHPExcel->getActiveSheet()->getCell('I5')->setValue('Total Price');
		$objPHPExcel->getActiveSheet()->getCell('J5')->setValue('Received Payment');
		$objPHPExcel->getActiveSheet()->getCell('K5')->setValue('Balance');
        
        $objPHPExcel->getActiveSheet()->getStyle('B5:C5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('D5:E5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('F5:G5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('H5:I5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('J5:K5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($main_title_style);
                
        $s          = 6;
        $i          = 1;
		$totquantity=0;
		$totprice=0;
		$totamount=0;
		$balance=0;
		foreach ($srchitemlist as $list) {
				$catname = '';
				$cusname = '';
				foreach($categorylist as $list1) { 
										if($list["category"]==$list1['category_id']){
										$catname = $list1['category_name'];
										}
									}
				foreach($cuslist as $list2) { 
										if($list["customer"]==$list2['id']){
										$cusname = $list2['name'];
										}
									}
                $objPHPExcel->getActiveSheet()->getCell('B'.$s)->setValue($i);
                $objPHPExcel->getActiveSheet()->getCell('C'.$s)->setValue($list['bill']);
                $objPHPExcel->getActiveSheet()->getCell('D'.$s)->setValue($catname); 
                $objPHPExcel->getActiveSheet()->getCell('E'.$s)->setValue($cusname);
                $objPHPExcel->getActiveSheet()->getCell('F'.$s)->setValue(PHPExcel_Shared_Date::PHPToExcel(strtotime($list['date'])));
                $objPHPExcel->getActiveSheet()->getStyle('F'.$s)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);
				$objPHPExcel->getActiveSheet()->getCell('G'.$s)->setValue($list['price']);
				$objPHPExcel->getActiveSheet()->getCell('H'.$s)->setValue($list['quantity']);
				$objPHPExcel->getActiveSheet()->getCell('I'.$s)->setValue($list['tprice']);
				$objPHPExcel->getActiveSheet()->getCell('J'.$s)->setValue($list['amount']);
				$objPHPExcel->getActiveSheet()->getCell('K'.$s)->setValue($list['balance']);
             $s++;
             $i++;
			$totquantity=$totquantity+$list['quantity'];
			$totprice=$totprice+$list['tprice'];
			$totamount=$totamount+$list['amount'];
			$balance=$balance+$list['balance'];
		}
			$objPHPExcel->getActiveSheet()->getCell('G'.$s)->setValue('Total');
			$objPHPExcel->getActiveSheet()->getCell('H'.$s)->setValue($totquantity);
			$objPHPExcel->getActiveSheet()->getCell('I'.$s)->setValue($totprice);
			$objPHPExcel->getActiveSheet()->getCell('J'.$s)->setValue($totamount);
			$objPHPExcel->getActiveSheet()->getCell('K'.$s)->setValue($balance);
			$objPHPExcel->getActiveSheet()->getStyle('G'.$s.':K'.$s)->applyFromArray($smalltitle_style);
			
			
        
		
        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$countRow.':E'.$countRow);
        // $objPHPExcel->getActiveSheet()->getCell('D'.$countRow)->setValue('Total');
        // $objPHPExcel->getActiveSheet()->getCell('F'.$countRow)->setValue($Stotal);

        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$countRow1.':E'.$countRow1);
        // $objPHPExcel->getActiveSheet()->getCell('D'.$countRow1)->setValue('Shipping');
        // $objPHPExcel->getActiveSheet()->getCell('F'.$countRow1)->setValue($shippingPrice);

        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$countRow2 .':E'.$countRow2);
        // $objPHPExcel->getActiveSheet()->getCell('D'.$countRow2)->setValue('Grand Total');
        // $objPHPExcel->getActiveSheet()->getCell('F'.$countRow2)->setValue($Gtotal);
            
             //////////////////////////////////////////////
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
// Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="OrderReport-'.date('Ymd h_i_s ').'.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
	}


	function xlsPrintOrder_srchven($type,$data,$srch){
		
		$CI     = get_instance();
        
        $ORDNO       = '#'.str_pad(1, 6, '0', STR_PAD_LEFT);

		$CI->load->model('purchaseadmin/Searchreport_Model', 'purchase');

        
       // $ORDNO       = '#'.str_pad(1, 6, '0', STR_PAD_LEFT);
		
        // Create new PHPExcel object
        $objPHPExcel            = new PHPExcel();
       // $orderProductsLists     = $CI->Settingmodel->orderProductsList($orderID);
        //$orderInfo              = $CI->Settingmodel->orderInfo($orderID);
        //$countRows              = count($orderProductsLists);

        // $Gtotal                 = $orderInfo->total;
        // $shippingPrice  = '';
        // $shippingPrice1 = 0;
        
		
        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                     ->setLastModifiedBy("Maarten Balliauw")
                                     ->setTitle("Office 2007 XLSX Test Document")
                                     ->setSubject("Office 2007 XLSX Test Document")
                                     ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                     ->setKeywords("office 2007 openxml php")
                                     ->setCategory("Test result file");


                $main_title_style = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => '31849b'),
                        'size'  => 20,
                        
                    ));
                $smalltitle_style = array(
                    'font'  => array(
                        'bold'  => true,
                        'size'  => 12
                    ),
                    'alignment' => array(
                            'wrap'       => true
                    )
                );
				
				
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true); 
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true); 
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true); 
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);

		
        $srchitemlist = $CI->purchase->srchitemlist($type,$srch);	
		$categorylist = $CI->purchase->categorylist();
		$venlist = $CI->purchase->venlist();
		
		$countRows = count($srchitemlist);
		
        $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B2:J2');
        $objPHPExcel->getActiveSheet()->getCell('B2')->setValue('Purchase Item Details');  
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D3:E3');
        $objPHPExcel->getActiveSheet()->getCell('D3')->setValue('Total no of Entries');
        $objPHPExcel->getActiveSheet()->getCell('F3')->setValue($countRows);   
        $objPHPExcel->getActiveSheet()->getCell('B5')->setValue('SI Number');
        $objPHPExcel->getActiveSheet()->getCell('C5')->setValue('Bill');
        $objPHPExcel->getActiveSheet()->getCell('D5')->setValue('Category');
		 $objPHPExcel->getActiveSheet()->getCell('E5')->setValue('Vendor Name');
        $objPHPExcel->getActiveSheet()->getCell('F5')->setValue('Date');
		$objPHPExcel->getActiveSheet()->getCell('G5')->setValue('Price');
		$objPHPExcel->getActiveSheet()->getCell('H5')->setValue('Quantity');
		$objPHPExcel->getActiveSheet()->getCell('I5')->setValue('Total Price');
		$objPHPExcel->getActiveSheet()->getCell('J5')->setValue('Received Payment');
		$objPHPExcel->getActiveSheet()->getCell('K5')->setValue('Balance');
        
        $objPHPExcel->getActiveSheet()->getStyle('B5:C5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('D5:E5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('F5:G5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('H5:I5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('J5:K5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($main_title_style);
                
        $s          = 6;
        $i          = 1;
		$totquantity=0;
		$totprice=0;
		$totamount=0;
		$balance=0;
		foreach ($srchitemlist as $list) {
				$catname = '';
				$cusname = '';
				foreach($categorylist as $list1) { 
										if($list["category"]==$list1['id']){
										$catname = $list1['catname'];
										}
									}
				foreach($venlist as $list2) { 
										if($list["vendor"]==$list2['id']){
										$cusname = $list2['name'];
										}
									}
                $objPHPExcel->getActiveSheet()->getCell('B'.$s)->setValue($i);
                $objPHPExcel->getActiveSheet()->getCell('C'.$s)->setValue($list['bill']);
                $objPHPExcel->getActiveSheet()->getCell('D'.$s)->setValue($catname); 
                $objPHPExcel->getActiveSheet()->getCell('E'.$s)->setValue($cusname);
                $objPHPExcel->getActiveSheet()->getCell('F'.$s)->setValue(PHPExcel_Shared_Date::PHPToExcel(strtotime($list['date'])));
                $objPHPExcel->getActiveSheet()->getStyle('F'.$s)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);
				$objPHPExcel->getActiveSheet()->getCell('G'.$s)->setValue($list['price']);
				$objPHPExcel->getActiveSheet()->getCell('H'.$s)->setValue($list['quantity']);
				$objPHPExcel->getActiveSheet()->getCell('I'.$s)->setValue($list['totprice']);
				$objPHPExcel->getActiveSheet()->getCell('J'.$s)->setValue($list['amount']);
				$objPHPExcel->getActiveSheet()->getCell('K'.$s)->setValue($list['balance']);
             $s++;
             $i++;
			 $totquantity=$totquantity+$list['quantity'];
			$totprice=$totprice+$list['totprice'];
			$totamount=$totamount+$list['amount'];
			$balance=$balance+$list['balance'];
		}
			$objPHPExcel->getActiveSheet()->getCell('G'.$s)->setValue('Total');
			$objPHPExcel->getActiveSheet()->getCell('H'.$s)->setValue($totquantity);
			$objPHPExcel->getActiveSheet()->getCell('I'.$s)->setValue($totprice);
			$objPHPExcel->getActiveSheet()->getCell('J'.$s)->setValue($totamount);
			$objPHPExcel->getActiveSheet()->getCell('K'.$s)->setValue($balance);
			$objPHPExcel->getActiveSheet()->getStyle('G'.$s.':K'.$s)->applyFromArray($smalltitle_style);
			
			
        
		
        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$countRow.':E'.$countRow);
        // $objPHPExcel->getActiveSheet()->getCell('D'.$countRow)->setValue('Total');
        // $objPHPExcel->getActiveSheet()->getCell('F'.$countRow)->setValue($Stotal);

        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$countRow1.':E'.$countRow1);
        // $objPHPExcel->getActiveSheet()->getCell('D'.$countRow1)->setValue('Shipping');
        // $objPHPExcel->getActiveSheet()->getCell('F'.$countRow1)->setValue($shippingPrice);

        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$countRow2 .':E'.$countRow2);
        // $objPHPExcel->getActiveSheet()->getCell('D'.$countRow2)->setValue('Grand Total');
        // $objPHPExcel->getActiveSheet()->getCell('F'.$countRow2)->setValue($Gtotal);
            
             //////////////////////////////////////////////
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
// Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="OrderReport-'.date('Ymd h_i_s ').'.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
	}
	function xlsPrintOrder_SalesPrice($value,$title){
		$count=count($value);
		$CI     = get_instance();
        
        $ORDNO       = '#'.str_pad(1, 6, '0', STR_PAD_LEFT);

        // Create new PHPExcel object
        $objPHPExcel            = new PHPExcel();
       // $orderProductsLists     = $CI->Settingmodel->orderProductsList($orderID);
        //$orderInfo              = $CI->Settingmodel->orderInfo($orderID);
        //$countRows              = count($orderProductsLists);

        // $Gtotal                 = $orderInfo->total;
        // $shippingPrice  = '';
        // $shippingPrice1 = 0;
        
        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                     ->setLastModifiedBy("Maarten Balliauw")
                                     ->setTitle("Office 2007 XLSX Test Document")
                                     ->setSubject("Office 2007 XLSX Test Document")
                                     ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                     ->setKeywords("office 2007 openxml php")
                                     ->setCategory("Test result file");


                $main_title_style = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => '31849b'),
                        'size'  => 20,
                        
                    ));
                $smalltitle_style = array(
                    'font'  => array(
                        'bold'  => true,
                        'size'  => 12
                    ),
                    'alignment' => array(
                            'wrap'       => true
                    )
                );
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true); 
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true); 
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
        //////////////////////////////////
        
        $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B2:F2');
        $objPHPExcel->getActiveSheet()->getCell('B2')->setValue($title);  
        //$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C3:D3');
        $objPHPExcel->getActiveSheet()->getCell('E3')->setValue('Total no of Entries');
        $objPHPExcel->getActiveSheet()->getCell('F3')->setValue($count);   
        $objPHPExcel->getActiveSheet()->getCell('B5')->setValue('S.No');
        $objPHPExcel->getActiveSheet()->getCell('C5')->setValue('Week Range');
        $objPHPExcel->getActiveSheet()->getCell('D5')->setValue('Item Type');
		
		$objPHPExcel->getActiveSheet()->getCell('E5')->setValue('Weekly Average Sale Price');
		$objPHPExcel->getActiveSheet()->getCell('F5')->setValue('Weekly Total Sale Price');
        
        $objPHPExcel->getActiveSheet()->getStyle('B5:C5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('D5:F5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($main_title_style);
                
        $s          = 6;        
        $i          = 1;
foreach ($value as $list) {
		$objPHPExcel->getActiveSheet()->getCell('B'.$s)->setValue($i);
		$objPHPExcel->getActiveSheet()->getCell('C'.$s)->setValue($list['0'].' to '.$list['1']);
		$objPHPExcel->getActiveSheet()->getCell('D'.$s)->setValue($list['categoryval']); 
		$val23=number_format($list['avgprice'],2);
		$objPHPExcel->getActiveSheet()->getCell('E'.$s)->setValue($val23);
		$val34=number_format($list['totprice'],2);
		$objPHPExcel->getActiveSheet()->getCell('F'.$s)->setValue($val34);
$s++;
$i++;
        }
       
		
        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$countRow1.':E'.$countRow1);
        // $objPHPExcel->getActiveSheet()->getCell('D'.$countRow1)->setValue('Shipping');
        // $objPHPExcel->getActiveSheet()->getCell('F'.$countRow1)->setValue($shippingPrice);

        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$countRow2 .':E'.$countRow2);
        // $objPHPExcel->getActiveSheet()->getCell('D'.$countRow2)->setValue('Grand Total');
        // $objPHPExcel->getActiveSheet()->getCell('F'.$countRow2)->setValue($Gtotal);
            
             //////////////////////////////////////////////
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
// Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="OrderReport-'.date('Ymd h_i_s ').'.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
	}
	function xlsPrintOrder_emp($data,$srch){
		//$count=count($value);
		$CI     = get_instance();
        $CI->load->model('superadmin/Employee_model', 'employee');	
		
		$emplist = $CI->employee->emplist();
		$count=count($emplist);
		
        $ORDNO       = '#'.str_pad(1, 6, '0', STR_PAD_LEFT);

        // Create new PHPExcel object
        $objPHPExcel            = new PHPExcel();
       // $orderProductsLists     = $CI->Settingmodel->orderProductsList($orderID);
        //$orderInfo              = $CI->Settingmodel->orderInfo($orderID);
        //$countRows              = count($orderProductsLists);

        // $Gtotal                 = $orderInfo->total;
        // $shippingPrice  = '';
        // $shippingPrice1 = 0;
        
        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                     ->setLastModifiedBy("Maarten Balliauw")
                                     ->setTitle("Office 2007 XLSX Test Document")
                                     ->setSubject("Office 2007 XLSX Test Document")
                                     ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                     ->setKeywords("office 2007 openxml php")
                                     ->setCategory("Test result file");


                $main_title_style = array(
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => '31849b'),
                        'size'  => 20,
                        
                    ));
                $smalltitle_style = array(
                    'font'  => array(
                        'bold'  => true,
                        'size'  => 12
                    ),
                    'alignment' => array(
                            'wrap'       => true
                    )
                );
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true); 
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true); 
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
        //////////////////////////////////
        
        $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B2:O2');
        $objPHPExcel->getActiveSheet()->getCell('B2')->setValue('Employee Details');  
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D3:E3');
        $objPHPExcel->getActiveSheet()->getCell('D3')->setValue('Total no of Entries');
        $objPHPExcel->getActiveSheet()->getCell('F3')->setValue($count);   
        $objPHPExcel->getActiveSheet()->getCell('B5')->setValue('SI Number');
        $objPHPExcel->getActiveSheet()->getCell('C5')->setValue('Name');
		
		 $objPHPExcel->getActiveSheet()->getCell('D5')->setValue('Nationality');
		 $objPHPExcel->getActiveSheet()->getCell('E5')->setValue('National ID');
		 $objPHPExcel->getActiveSheet()->getCell('F5')->setValue('Hire Date');
		 
        $objPHPExcel->getActiveSheet()->getCell('G5')->setValue('Email');
		 $objPHPExcel->getActiveSheet()->getCell('H5')->setValue('Mobile');
        $objPHPExcel->getActiveSheet()->getCell('I5')->setValue('ID No');
		$objPHPExcel->getActiveSheet()->getCell('J5')->setValue('Salary');
		$objPHPExcel->getActiveSheet()->getCell('K5')->setValue('Job');
		
		$objPHPExcel->getActiveSheet()->getCell('L5')->setValue('Notes');
		$objPHPExcel->getActiveSheet()->getCell('M5')->setValue('Bank Name');
		$objPHPExcel->getActiveSheet()->getCell('N5')->setValue('IBAN Number');
		$objPHPExcel->getActiveSheet()->getCell('O5')->setValue('Status');
		
		$objPHPExcel->getActiveSheet()->getCell('P5')->setValue('Role');
		//$objPHPExcel->getActiveSheet()->getCell('J5')->setValue('Note');
        
        $objPHPExcel->getActiveSheet()->getStyle('B5:C5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('D5:E5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('F5:G5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('H5:I5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('J5:K5')->applyFromArray($smalltitle_style);
		$objPHPExcel->getActiveSheet()->getStyle('K5:L5')->applyFromArray($smalltitle_style);
		$objPHPExcel->getActiveSheet()->getStyle('L5:M5')->applyFromArray($smalltitle_style);
		$objPHPExcel->getActiveSheet()->getStyle('M5:N5')->applyFromArray($smalltitle_style);
		$objPHPExcel->getActiveSheet()->getStyle('N5:O5')->applyFromArray($smalltitle_style);
		$objPHPExcel->getActiveSheet()->getStyle('O5:P5')->applyFromArray($smalltitle_style);
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($main_title_style);
                
        $s          = 6;
        $i          = 1;

		

		foreach ($emplist as $list) {
				
				if($list['status']==0) { $sts = 'Active'; } else if($list['status']==1) { $sts = 'Vacation'; } else if($list['status']==2) { $sts = 'Quit'; }
				if($list['role']==1){
					  $role='Super Admin';
				  } else if($list['role']==2){
					  $role='Sales Admin';
				  } else if($list['role']==3){
					  $role='Purchase Admin';
				  } else if($list['role']==4){
					  $role='Operation Admin';
				  } else{
					  $role='Operation Employee';
				  }

				  $objPHPExcel->getActiveSheet()->getCell('B'.$s)->setValue($i);
                $objPHPExcel->getActiveSheet()->getCell('C'.$s)->setValue($list['name']);
				
				$objPHPExcel->getActiveSheet()->getCell('D'.$s)->setValue($list['nationality']);
				$objPHPExcel->getActiveSheet()->getCell('E'.$s)->setValue($list['nationid']);
				$objPHPExcel->getActiveSheet()->getCell('F'.$s)->setValue($list['hiredate']);
				
                $objPHPExcel->getActiveSheet()->getCell('G'.$s)->setValue($list['email']); 
                $objPHPExcel->getActiveSheet()->getCell('H'.$s)->setValue($list['mobileno']);
                $objPHPExcel->getActiveSheet()->getCell('I'.$s)->setValue($list['idcard']);
				$objPHPExcel->getActiveSheet()->getCell('J'.$s)->setValue($list['salary']);
				$objPHPExcel->getActiveSheet()->getCell('K'.$s)->setValue($list['job']);
				
				$objPHPExcel->getActiveSheet()->getCell('L'.$s)->setValue($list['notes']);
				$objPHPExcel->getActiveSheet()->getCell('M'.$s)->setValue($list['bankname']);
				$objPHPExcel->getActiveSheet()->getCell('N'.$s)->setValue($list['iban']);
				
				$objPHPExcel->getActiveSheet()->getCell('O'.$s)->setValue($sts);
				$objPHPExcel->getActiveSheet()->getCell('P'.$s)->setValue($role);
				//$objPHPExcel->getActiveSheet()->getCell('J'.$s)->setValue($list['amount']);
             $s++;
             $i++;
			 
		
		}
       
		
        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$countRow1.':E'.$countRow1);
        // $objPHPExcel->getActiveSheet()->getCell('D'.$countRow1)->setValue('Shipping');
        // $objPHPExcel->getActiveSheet()->getCell('F'.$countRow1)->setValue($shippingPrice);

        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$countRow2 .':E'.$countRow2);
        // $objPHPExcel->getActiveSheet()->getCell('D'.$countRow2)->setValue('Grand Total');
        // $objPHPExcel->getActiveSheet()->getCell('F'.$countRow2)->setValue($Gtotal);
            
             //////////////////////////////////////////////
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
// Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="OrderReport-'.date('Ymd h_i_s ').'.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
	}
	?>